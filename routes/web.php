<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();


Route::get('/bahan-kajian/grid', 'BahanKajianController@grid');
Route::resource('/bahan-kajian', 'BahanKajianController');
Route::get('/blooms-taxonomy/grid', 'BloomsTaxonomyController@grid');
Route::resource('/blooms-taxonomy', 'BloomsTaxonomyController');
Route::get('/cpl/grid', 'CplController@grid');
Route::resource('/cpl', 'CplController');
Route::get('/mata-kuliah/grid', 'MataKuliahController@grid');
Route::resource('/mata-kuliah', 'MataKuliahController');
Route::get('/okupasi/grid', 'OkupasiController@grid');
Route::resource('/okupasi', 'OkupasiController');
Route::get('/permission-role/grid', 'PermissionRoleController@grid');
Route::resource('/permission-role', 'PermissionRoleController');
Route::get('/permission/grid', 'PermissionController@grid');
Route::resource('/permission', 'PermissionController');
Route::get('/prodi/grid', 'ProdiController@grid');
Route::resource('/prodi', 'ProdiController');
Route::get('/profil-lulusan/grid', 'ProfilLulusanController@grid');
Route::resource('/profil-lulusan', 'ProfilLulusanController');
Route::get('/ranah-topik/grid', 'RanahTopikController@grid');
Route::resource('/ranah-topik', 'RanahTopikController');
Route::get('/rel-cpl-bk/grid', 'RelCplBkController@grid');
Route::resource('/rel-cpl-bk', 'RelCplBkController');
Route::get('/rel-topik-bk/grid', 'RelTopikBkController@grid');
Route::resource('/rel-topik-bk', 'RelTopikBkController');
Route::get('/rel-topik-mk/grid', 'RelTopikMkController@grid');
Route::resource('/rel-topik-mk', 'RelTopikMkController');
Route::get('/role/grid', 'RoleController@grid');
Route::resource('/role', 'RoleController');
Route::get('/user-prodi/grid', 'UserProdiController@grid');
Route::resource('/user-prodi', 'UserProdiController');
Route::get('/user-role/grid', 'RoleUserController@grid');
Route::resource('/user-role', 'RoleUserController');
Route::get('/users/grid', 'UserController@grid');
Route::resource('/users', 'UserController');