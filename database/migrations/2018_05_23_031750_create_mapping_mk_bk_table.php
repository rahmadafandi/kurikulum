<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMappingMkBkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mapping_mk_bk', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('profil_id');
			$table->char('periode', 7)->comment('format: yyyy.** where ** = periode, misal semester atau trimester');
			$table->integer('max_sks')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mapping_mk_bk');
	}

}
