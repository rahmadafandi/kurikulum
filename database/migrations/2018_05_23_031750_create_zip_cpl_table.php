<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateZipCplTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('zip_cpl', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('master_id')->comment('ref to original cpl');
			$table->boolean('jenis_id');
			$table->integer('profil_id')->index('profil_id');
			$table->text('deskripsi', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('zip_cpl');
	}

}
