<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStrukturKurikulumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('struktur_kurikulum', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('profil_id')->index('profil_id');
			$table->integer('mapping_id')->index('mapping_id');
			$table->char('periode', 7)->comment('format sama dengan periode mapping');
			$table->integer('max_sks')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('struktur_kurikulum');
	}

}
