<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCplTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cpl', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('jenis_id')->index('jenis_id');
			$table->integer('okupasi_id')->nullable()->index('profil_id');
			$table->text('deskripsi', 65535)->nullable();
			$table->decimal('bobot', 4)->default(1.00);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cpl');
	}

}
