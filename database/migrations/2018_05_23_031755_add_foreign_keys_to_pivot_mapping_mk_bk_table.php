<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPivotMappingMkBkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pivot_mapping_mk_bk', function(Blueprint $table)
		{
			$table->foreign('mapping_id', 'pivot_mapping_mk_bk_ibfk_1')->references('id')->on('mapping_mk_bk')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('mk_id', 'pivot_mapping_mk_bk_ibfk_2')->references('id')->on('mata_kuliah')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('bk_id', 'pivot_mapping_mk_bk_ibfk_3')->references('id')->on('bahan_kajian')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pivot_mapping_mk_bk', function(Blueprint $table)
		{
			$table->dropForeign('pivot_mapping_mk_bk_ibfk_1');
			$table->dropForeign('pivot_mapping_mk_bk_ibfk_2');
			$table->dropForeign('pivot_mapping_mk_bk_ibfk_3');
		});
	}

}
