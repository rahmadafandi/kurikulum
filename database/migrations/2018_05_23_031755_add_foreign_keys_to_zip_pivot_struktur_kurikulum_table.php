<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToZipPivotStrukturKurikulumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('zip_pivot_struktur_kurikulum', function(Blueprint $table)
		{
			$table->foreign('id', 'zip_pivot_struktur_kurikulum_ibfk_1')->references('id')->on('pivot_struktur_kurikulum')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('zip_pivot_struktur_kurikulum', function(Blueprint $table)
		{
			$table->dropForeign('zip_pivot_struktur_kurikulum_ibfk_1');
		});
	}

}
