<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePivotStrukturKurikulumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pivot_struktur_kurikulum', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('struktur_id')->index('struktur_id');
			$table->integer('mk_id')->index('mk_id');
			$table->integer('bk_id')->index('bk_id');
			$table->integer('sks')->unsigned();
			$table->char('periode', 2)->comment('format: 2 digit periode, misal semester atau trimester');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pivot_struktur_kurikulum');
	}

}
