<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCplTemplateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cpl_template', function(Blueprint $table)
		{
			$table->foreign('jenis_id', 'cpl_template_ibfk_1')->references('id')->on('jenis_cpl')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cpl_template', function(Blueprint $table)
		{
			$table->dropForeign('cpl_template_ibfk_1');
		});
	}

}
