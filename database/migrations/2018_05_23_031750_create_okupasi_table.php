<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOkupasiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('okupasi', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nama', 100);
			$table->text('deskripsi', 65535)->nullable();
			$table->integer('profil_lulusan_id')->nullable()->index('profil_lulusan_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('okupasi');
	}

}
