<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRelTopikMkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rel_topik_mk', function(Blueprint $table)
		{
			$table->foreign('mk_id', 'rel_topik_mk_ibfk_1')->references('id')->on('mata_kuliah')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('topik_id', 'rel_topik_mk_ibfk_2')->references('id')->on('ranah_topik')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rel_topik_mk', function(Blueprint $table)
		{
			$table->dropForeign('rel_topik_mk_ibfk_1');
			$table->dropForeign('rel_topik_mk_ibfk_2');
		});
	}

}
