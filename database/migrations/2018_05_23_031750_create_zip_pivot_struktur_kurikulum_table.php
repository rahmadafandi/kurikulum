<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateZipPivotStrukturKurikulumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('zip_pivot_struktur_kurikulum', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('mk_id');
			$table->string('mk_nama', 100);
			$table->integer('bk_id');
			$table->string('bk_nama', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('zip_pivot_struktur_kurikulum');
	}

}
