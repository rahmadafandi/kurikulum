<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProdiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_prodi', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned();
			$table->integer('prodi_id')->index('prodi_id');
			$table->primary(['user_id','prodi_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_prodi');
	}

}
