<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCplTemplateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cpl_template', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('jenis_id')->index('jenis_id');
			$table->text('deskripsi', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cpl_template');
	}

}
