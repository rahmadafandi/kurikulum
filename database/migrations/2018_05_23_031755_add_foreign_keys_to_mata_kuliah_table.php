<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMataKuliahTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mata_kuliah', function(Blueprint $table)
		{
			$table->foreign('jenis_id', 'mata_kuliah_ibfk_1')->references('id')->on('jenis_mk')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mata_kuliah', function(Blueprint $table)
		{
			$table->dropForeign('mata_kuliah_ibfk_1');
		});
	}

}
