<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStrukturKurikulumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('struktur_kurikulum', function(Blueprint $table)
		{
			$table->foreign('mapping_id', 'struktur_kurikulum_ibfk_1')->references('id')->on('mapping_mk_bk')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('profil_id', 'struktur_kurikulum_ibfk_2')->references('id')->on('zip_profil_lulusan')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('struktur_kurikulum', function(Blueprint $table)
		{
			$table->dropForeign('struktur_kurikulum_ibfk_1');
			$table->dropForeign('struktur_kurikulum_ibfk_2');
		});
	}

}
