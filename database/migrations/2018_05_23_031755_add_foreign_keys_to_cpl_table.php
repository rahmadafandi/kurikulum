<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCplTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cpl', function(Blueprint $table)
		{
			$table->foreign('jenis_id', 'cpl_ibfk_1')->references('id')->on('jenis_cpl')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('okupasi_id', 'cpl_ibfk_2')->references('id')->on('okupasi')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cpl', function(Blueprint $table)
		{
			$table->dropForeign('cpl_ibfk_1');
			$table->dropForeign('cpl_ibfk_2');
		});
	}

}
