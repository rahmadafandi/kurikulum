<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPivotStrukturKurikulumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pivot_struktur_kurikulum', function(Blueprint $table)
		{
			$table->foreign('struktur_id', 'pivot_struktur_kurikulum_ibfk_1')->references('id')->on('struktur_kurikulum')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pivot_struktur_kurikulum', function(Blueprint $table)
		{
			$table->dropForeign('pivot_struktur_kurikulum_ibfk_1');
		});
	}

}
