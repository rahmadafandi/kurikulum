<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePivotMappingMkBkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pivot_mapping_mk_bk', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('mapping_id')->index('mapping_id');
			$table->integer('mk_id')->index('mk_id');
			$table->integer('bk_id')->index('bk_id');
			$table->integer('bobot_bk')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pivot_mapping_mk_bk');
	}

}
