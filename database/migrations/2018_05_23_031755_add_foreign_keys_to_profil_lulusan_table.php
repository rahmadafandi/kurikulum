<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProfilLulusanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profil_lulusan', function(Blueprint $table)
		{
			$table->foreign('prodi_id', 'profil_lulusan_ibfk_1')->references('id')->on('prodi')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('profil_lulusan', function(Blueprint $table)
		{
			$table->dropForeign('profil_lulusan_ibfk_1');
		});
	}

}
