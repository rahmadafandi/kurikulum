<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToZipCplTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('zip_cpl', function(Blueprint $table)
		{
			$table->foreign('profil_id', 'zip_cpl_ibfk_1')->references('id')->on('zip_profil_lulusan')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('zip_cpl', function(Blueprint $table)
		{
			$table->dropForeign('zip_cpl_ibfk_1');
		});
	}

}
