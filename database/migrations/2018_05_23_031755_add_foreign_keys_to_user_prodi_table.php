<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserProdiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_prodi', function(Blueprint $table)
		{
			$table->foreign('prodi_id', 'user_prodi_ibfk_2')->references('id')->on('prodi')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id', 'user_prodi_ibfk_3')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_prodi', function(Blueprint $table)
		{
			$table->dropForeign('user_prodi_ibfk_2');
			$table->dropForeign('user_prodi_ibfk_3');
		});
	}

}
