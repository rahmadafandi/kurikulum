<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRelTopikMkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rel_topik_mk', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('topik_id')->index('topik_id');
			$table->integer('mk_id')->index('mk_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rel_topik_mk');
	}

}
