<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilLulusanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profil_lulusan', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('prodi_id')->index('prodi_id');
			$table->string('nama', 100);
			$table->text('deskripsi', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profil_lulusan');
	}

}
