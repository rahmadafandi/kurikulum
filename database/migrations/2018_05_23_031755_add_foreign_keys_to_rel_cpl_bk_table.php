<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRelCplBkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rel_cpl_bk', function(Blueprint $table)
		{
			$table->foreign('bk_id', 'rel_cpl_bk_ibfk_1')->references('id')->on('bahan_kajian')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cpl_id', 'rel_cpl_bk_ibfk_2')->references('id')->on('cpl')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rel_cpl_bk', function(Blueprint $table)
		{
			$table->dropForeign('rel_cpl_bk_ibfk_1');
			$table->dropForeign('rel_cpl_bk_ibfk_2');
		});
	}

}
