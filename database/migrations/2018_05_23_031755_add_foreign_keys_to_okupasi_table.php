<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOkupasiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('okupasi', function(Blueprint $table)
		{
			$table->foreign('profil_lulusan_id', 'okupasi_ibfk_1')->references('id')->on('profil_lulusan')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('okupasi', function(Blueprint $table)
		{
			$table->dropForeign('okupasi_ibfk_1');
		});
	}

}
