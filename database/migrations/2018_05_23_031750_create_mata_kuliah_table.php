<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMataKuliahTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mata_kuliah', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('jenis_id')->index('jenis_id');
			$table->string('nama', 50);
			$table->text('deskripsi', 65535)->nullable();
			$table->text('dependency_path', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mata_kuliah');
	}

}
