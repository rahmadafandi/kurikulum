<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRelCplBkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rel_cpl_bk', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('cpl_id')->index('rel_cpl_bk_ibfk_2');
			$table->integer('bk_id')->index('rel_cpl_bk_ibfk_1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rel_cpl_bk');
	}

}
