<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mata_kuliah', function(Blueprint $table)
        {
            $table->boolean('lingkup_id')->index('lingkup_id');
            $table->foreign('lingkup_id', 'mata_kuliah_ibfk_2')->references('id')->on('lingkup_mk')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mata_kuliah', function(Blueprint $table)
        {
            $table->dropForeign('mata_kuliah_ibfk_2');
        });
    }
}
