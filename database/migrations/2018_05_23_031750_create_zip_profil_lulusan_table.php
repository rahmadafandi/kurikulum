<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateZipProfilLulusanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('zip_profil_lulusan', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('master_id')->comment('ref to original profil lulusan');
			$table->integer('prodi_id');
			$table->string('nama', 100);
			$table->text('deskripsi', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('zip_profil_lulusan');
	}

}
