<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBloomsTaxonomyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blooms_taxonomy', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('frasa', 50);
			$table->boolean('level')->default(1);
			$table->string('kata_dasar', 20)->nullable()->comment('tanpa imbuhan');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blooms_taxonomy');
	}

}
