<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisMkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('jenis_mk')->insert([
            ['id' => 1, 'nama' => 'Umum'],
            ['id' => 2, 'nama' => 'Khusus'],
            ['id' => 3, 'nama' => 'Pilihan']
        ]);
    }
}
