<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert(['name' => 'superadmin','email' => 'admin@admin.com','password' => bcrypt('iniadmin')]);
    	DB::table('users')->insert(['name' => 'gagas','email' => 'gagas@gagas.com','password' => bcrypt('inigagas')]);
    	DB::table('users')->insert(['name' => 'rahmad','email' => 'rahmad@rahmad.com','password' => bcrypt('inirahmad')]);
    	DB::table('roles')->insert(['name'=>'superadmin','display_name'=>'Super Admin','description'=>'Manajemen user dan konten']);
    	DB::table('permissions')->insert(['name'=>'superadmin','display_name'=>strtoupper('Super Admin'),'description'=>strtoupper('Manajemen user dan konten')]);
    	$permissions = DB::select('select id from permissions');
        $superadmin = DB::select("select id from roles where name='superadmin'");
        foreach ($permissions as $value) {
            DB::table('permission_role')->insert(['permission_id'=>$value->id,'role_id'=>$superadmin[0]->id]);
        }

        $useradmin = DB::select("select id from users where name='superadmin'");
        DB::table('role_user')->insert(['user_id'=>$useradmin[0]->id,'role_id'=>$superadmin[0]->id]);
    }
}
