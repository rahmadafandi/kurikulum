<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LingkupMkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('lingkup_mk')->insert([
            ['id' => 1, 'nama' => 'Universiter'],
            ['id' => 2, 'nama' => 'Fakultas'],
            ['id' => 3, 'nama' => 'Jurusan'],
            ['id' => 4, 'nama' => 'Prodi']
        ]);
    }
}
