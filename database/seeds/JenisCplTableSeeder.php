<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisCplTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('jenis_cpl')->insert([
            ['id' => 1, 'nama' => 'Sikap'],
            ['id' => 2, 'nama' => 'Pengetahuan'],
            ['id' => 3, 'nama' => 'Keterampilan Umum'],
            ['id' => 4, 'nama' => 'Keterampilan Khusus']
        ]);
    }
}
