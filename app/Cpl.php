<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cpl extends Model
{
    //
    protected $table = 'cpl';
    public $timestamps = false;

    public function jenis(){
    	return $this->belongsTo(JenisCpl::class,'jenis_id');
    }

    public function okupasi(){
    	return $this->belongsTo(Okupasi::class,'okupasi_id');
    }

}