<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RanahTopik extends Model
{
    //
    protected $table = 'ranah_topik';
    public $timestamps = false;
    protected $fillable = ['nama'];

    public function mata_kuliah()
    {
        return $this->belongsToMany('App\MataKuliah', 'rel_topik_mk', 'topik_id', 'mk_id');
    }

    public function bahan_kajian()
    {
        return $this->belongsToMany('App\BahanKajian', 'rel_topik_bk', 'topik_id', 'bk_id');
    }
}