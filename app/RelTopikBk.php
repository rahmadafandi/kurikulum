<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelTopikBk extends Model
{
    //
    protected $table = 'rel_topik_bk';
    public $timestamps = false;

    public function bk(){
    	return $this->belongsTo(BahanKajian::class,'bk_id');
    }

    public function topik(){
    	return $this->belongsTo(RanahTopik::class,'topik_id');
    }
}