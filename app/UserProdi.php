<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProdi extends Model
{
    //
    protected $table = 'user_prodi';
    public $timestamps = false;
    public $primaryKey = 'user_id';

    public function user(){
    	return $this->belongsTo(User::class,'user_id');
    }
 
    public function prodi(){
    	return $this->belongsTo(Prodi::class,'prodi_id');
    }
}