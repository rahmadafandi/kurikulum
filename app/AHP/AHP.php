<?php

namespace App\AHP;

use App\AHP\SimpleAHP;

class AHP extends SimpleAHP
{
	/**
	 * A simple AHP class to help decide from a set of criterias (and maybe
	 * subcriterias).
	 */

	/**
	 * The goals, or subjects, which you are deciding upon.
	 */
	protected $goals = [];

	/**
	 * The criterias, as well as their weights for the pairwise relation
	 * (relation between two) of criteria.
	 * Define each criteria as an array:
	 * [
	 * 	'name'		required	the name of the criteria
	 * 	'relation'	required	the weight of the criteria
	 * 	'sub'		optional	any subcriterias, defined the same way as a criteria
	 * ]
	 * Looking at the array above, it means that the criterias could be defined
	 * as a recursive-like array.
	 * Any recursion will later be calculated its end value, and thus flattened
	 * as in the last criteria diagram in the following:
	 * https://en.wikipedia.org/wiki/Analytic_hierarchy_process_%E2%80%93_car_example
	 * ...and thus, only the weights of end branches (a.k.a leaf) are used.
	 * The weights are better defined as a combination of two. A weight is
	 * an integer in the range of 1-9, with 1 as equal importance and 9 as
	 * totally incomparable.
	 * The relation is defined in left to right direction, that is, earlier criteria
	 * first. For an AB relation that weights B more than A (e.g 3 or 5 for B),
	 * define the weight as 1/weight, e.g 1/3 or 1/5. The relation is the relation
	 * of same level elements in the same scope.
	 * An example would explain better:
	 * Consider criterias A, B, C, D, and E,
	 * then the weights would be:
	 * $weights = [
	 * 	[ relation => [3,1,1,2] ],		// for relation of A with BCDE
	 * 	[ relation => [1,1/6,3] ],		// for relation of B with CDE
	 * 	[ relation => [9,1/3] ],		// for relation of C with DE
	 * 	[ relation => [7] ]				// for relation of D with E
	 * ];
	 * By the end of the last element above, every pairwise relation has been
	 * defined, since e.g the pairwise relation of EA = 1/AE. Hence no relation
	 * array for the last entry.
	 * The weights array above will be converted to a proper matrix for the
	 * pairwise relations.
	 * If a proper weights permutation is not supplied and there are some undefined
	 * pairwise relations, a relation of weight 1 (equally important) is assumed.
	 */
	protected $criterias = [];

	/**
	 * Will contain the simplified end criterias, each with their own weight.
	 * The scores array must follow the criterias in this array. Define each
	 * criteria's permutation with the key of the end_criteria.
	 * See function getEndCriterias().
	 */
	protected $end_criterias = [];

	/**
	 * Scores for each goal in each criteria.
	 * The format is exactly the same as the relation array, i.e as a permutation,
	 * but defined in one array.
	 * The pairwise relation is between any two goals instead of criterias.
	 * The only difference here is that for every criteria, you'd have an entry of scores array.
	 * Define each scores array with the key of each criteria, e.g price => [scores].
	 * Example:
	 * Consider goals [A, B, C, D, E], and end_criterias [W, X, Y, Z]
	 * $weights = [
	 * 	'W' => [
	 * 		[3,1,1,2],		// for relation of A with BCDE
	 * 		[1,1/6,3],		// for relation of B with CDE
	 * 		[9,1/3],		// for relation of C with DE
	 * 		[7],			// for relation of D with E
	 * 	],
	 * 	'X' => [...]
	 * ];
	 */
	protected $scores = [];

	public function __construct($goals = [], $criterias = [], $scores = []) {
		$this->setGoals($goals);
		$this->setCriterias($criterias);
		$this->setScores($scores);
	}

	public function setGoals($goals) {
		$this->goals = array_values($goals);
	}

	public function setCriterias($criterias) {
		$this->criterias = $this->buildRelationMatrix($criterias);
	}

	protected function buildRelationMatrix($criterias, $parent_weight = null, $parent_path = null) {
		$criterias = array_values($criterias);
		$default = 1;
		$matrix = [];
		$num_related = count($criterias);
		foreach($criterias as $c => $criteria) {
			$c1 = $c + 1;
			$relation = isset($criteria['relation']) ? $criteria['relation'] : array_fill(0, $num_related - $c1, $default);
			for($i = 0; $i < $num_related - $c1; $i++) {
				$j = $i + $c1;
				$val = isset($relation[$i]) ? $relation[$i] : $default;
				$matrix[$c][$j] = intval($val);
				$matrix[$j][$c] = 1 / $val; // the opposite direction for the relation
			}
		}
		for($i = 0; $i < $num_related; $i++) {
			$matrix[$i][$i] = 1; // relation with self
		}
		foreach($matrix as $r => $relations) {
			ksort($matrix[$r]);
		}

		/**
		 * Calculate the relative matrix, then the relative weights.
		 * The calculation formula used here is pretty simple, refers to
		 * http://mat.gsia.cmu.edu/classes/mstc/multiple/node4.html
		 */
		$col_sum = [];
		for($i = 0; $i < $num_related; $i++) {
			$col_sum[$i] = array_sum(array_pluck($matrix, $i));
		}

		$relative_matrix = [];
		for($i = 0; $i < $num_related; $i++) {
			for($j = 0; $j < $num_related; $j++) {
				$relative_matrix[$i][$j] = $matrix[$i][$j] / $col_sum[$j];
			}
		}

		$relative = [];
		for($i = 0; $i < $num_related; $i++) {
			$path = '';
			if($parent_path != null)
				$path = $parent_path .'.';
			$path .= $criterias[$i]['name'];

			$criterias[$i]['weight'] = array_sum($relative_matrix[$i]) / count($relative_matrix[$i]);
			$criterias[$i]['relative_weight'] = $criterias[$i]['weight'];
			if($parent_weight != null) {
				$criterias[$i]['relative_weight'] *= $parent_weight;
			}

			// If there are any subcriterias, we need to resolve their weight first.
			if(!empty($criterias[$i]['sub'])) {
				$criterias[$i]['sub'] = $this->buildRelationMatrix($criterias[$i]['sub'], $criterias[$i]['relative_weight'], $path);
			} else {
				// No subcriterias? Which means the criteria is an end criteria.
				$this->end_criterias[$path] = [
					'name' => $criterias[$i]['name'],
					'path' => $path,
					'weight' => $criterias[$i]['relative_weight']
				];
			}
		}

		return $criterias;
	}

	/**
	 * You can use this function to check on the end criterias in case
	 * you only made the criteria tree without checking what the end criterias
	 * are and how much they contribute to the final overall value.
	 */
	public function getEndCriterias() {
		return $this->end_criterias;
	}

	public function setScores($scores) {
		$scores_matrices = [];
		$scores_relative = [];
		foreach($this->end_criterias as $key => $criteria) {
			$scores_matrices[$key] = $this->permutationToMatrix($scores[$key], count($this->goals));
		}

		$this->scores = $scores_matrices;
	}

	// Refers to http://mat.gsia.cmu.edu/classes/mstc/multiple/node4.html
	protected function permutationToMatrix($permutation, $num_related) {
		$default = 1;
		$matrix = [];
		$permutation = array_values($permutation);

		for($i = 0; $i < $num_related - 1; $i++) {
			$j = $i + 1;

			$rels = isset($permutation[$i]) ? $permutation[$i] : array_fill(0, $num_related - $j, $default);
			for($ii = 0; $ii < $num_related - $j; $ii++) {
				$jj = $ii + $j;
				$val = isset($rels[$ii]) ? $rels[$ii] : $default;
				$matrix[$i][$jj] = $val;
				$matrix[$jj][$i] = 1 / $val; // the opposite direction for the relation
			}
		}
		for($i = 0; $i < $num_related; $i++) {
			$matrix[$i][$i] = 1; // relation with self
		}
		foreach($matrix as $s => $rels) {
			ksort($matrix[$s]);
		}

		return $matrix;
	}

	// Refers to http://mat.gsia.cmu.edu/classes/mstc/multiple/node4.html
	protected function matrixRatioToRelative($matrix) {
		$num = count($matrix);
		$col_sum = [];
		for($j = 0; $j < $num; $j++) {
			$col_sum[$j] = array_sum(array_pluck($matrix, $j));
		}

		$relative_matrix = [];
		for($i = 0; $i < $num; $i++) {
			for($j = 0; $j < $num; $j++) {
				$relative_matrix[$i][$j] = $matrix[$i][$j] / $col_sum[$j];
			}
		}
		foreach($matrix as $r => $relation) {
			for($j = 0; $j < $num; $j++) {
				$relative_matrix[$r][$j] = $matrix[$r][$j] / $col_sum[$j];
			}
		}

		$relative = [];
		foreach($matrix as $r => $relation) {
			$relative[$r] = array_sum($relative_matrix[$r]) / count($relative_matrix[$r]);
		}

		return array($relative_matrix, $relative);
	}

	public function decide($get_overall_score = false) {
		// From ratios to relative
		$relative_scores_matrix = [];
		$relative_scores = [];
		foreach($this->scores as $key => $score) {
			list($relative_scores_matrix[$key], $relative_scores[$key]) = $this->matrixRatioToRelative($score);
		}

		// Get the overall (final) score for each goal
		$overall_score = [];
		for($i = 0; $i < count($this->goals); $i++) {
			$overall_score[$i] = 0;
			foreach($this->end_criterias as $path => $criteria) {
				$overall_score[$i] += $relative_scores[$path][$i] * $this->end_criterias[$path]['weight'];
			}
		}

		// Combine the goals and scores, and get the max
		$overall_score = array_combine($this->goals, $overall_score);
		$max = collect($overall_score)->max();

		if($get_overall_score) {
			return collect($overall_score)->sort()->reverse()->all();
		}

		$key = array_search($max, $overall_score);

		return $key;
	}

	public function matrixToTable($matrix) {
		$html = '<table border="1"><tbody>';
		foreach($matrix as $rels) {
			$html .= '<tr>';
			foreach($rels as $score) {
				$html .= '<td style="width: 100px;">'.$score.'</td>';
			}
			$html .= '</tr>';
		}
		$html .= '</tbody></table>';
		return $html;
	}
}
