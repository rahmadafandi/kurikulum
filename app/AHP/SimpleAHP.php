<?php

namespace App\AHP;

class SimpleAHP
{
	/**
	 * A simple AHP class to help decide from a set of criterias.
	 * Not recommended for the use of subcriterias.
	 */

	/**
	 * The goals, or subjects, which you are deciding upon.
	 */
	protected $goals = [];

	/**
	 * Criterias, in the form of one-dimensional array.
	 * For subcriterias, you must resolve the end criterias for each; for example
	 * look at the last criteria diagram in the following:
	 * https://en.wikipedia.org/wiki/Analytic_hierarchy_process_%E2%80%93_car_example
	 */
	protected $criterias = [];

	/**
	 * Weights for the pairwise relation (relation between two) of criteria,
	 * in the form of two-dimensional array. The weights are better defined as
	 * a combination of two. A weight is an integer in the range of 1-9, with
	 * 1 as equal importance and 9 as totally incomparable.
	 * The relation is defined in left to right direction, that is, earlier criteria
	 * first. For an AB relation that weights B more than A (e.g 3 or 5 for B),
	 * define the weight as 1/weight, e.g 1/3 or 1/5.
	 * An example would explain better:
	 * Consider criterias [A,B,C,D,E],
	 * then the weights would be:
	 * $weights = [
	 * 	[3,1,1,2],			// for relation of A with BCDE
	 * 	[1,1/6,3],			// for relation of B with CDE
	 * 	[9,1/3],			// for relation of C with DE
	 * 	[7]					// for relation of D with E
	 * ];
	 * By the end of the last element above, every pairwise relation has been
	 * defined, since e.g the pairwise relation of EA = 1/AE.
	 * The weights array above will be converted to a proper matrix for the
	 * pairwise relations.
	 * If a proper weights permutation is not supplied and there are some undefined
	 * pairwise relations, a relation of weight 1 (equally important) is assumed.
	 */
	protected $weights = [];
	/**
	 * Will contain the calculated relative weights matrix
	 */
	// protected $relative_weights = [];

	/**
	 * Scores for each goal in each criteria.
	 * The format is exactly the same as the weights array. The pairwise relation
	 * is between any two goals instead of criterias. The only difference here
	 * is that for every criteria, you'd have an entry of scores array.
	 * Define each scores array with the key of each criteria, e.g price => [scores].
	 */
	protected $scores = [];
	/**
	 * Will contain the calculated relative scores matrices
	 */
	// protected $relative_scores = [];

	public function __construct($goals = [], $criterias = [], $weights = [], $scores = []) {
		$this->setGoals($goals);
		$this->setCriterias($criterias);
		$this->setWeights($weights);
		$this->setScores($scores);
	}

	public function setGoals($goals) {
		$this->goals = array_values($goals);
	}

	public function setCriterias($criterias) {
		$this->criterias = array_values($criterias);
	}

	public function setWeights($weights) {
		$this->weights = $this->permutationToMatrix($weights, count($this->criterias));
	}

	public function setScores($scores) {
		$scores_matrices = [];
		$scores_relative = [];
		$num = count($this->criterias);
		for($i = 0; $i < $num; $i++) {
			$scores_matrices[$i] = $this->permutationToMatrix($scores[$i], count($this->goals));
		}

		$this->scores = $scores_matrices;
	}

	// Refers to http://mat.gsia.cmu.edu/classes/mstc/multiple/node4.html
	protected function permutationToMatrix($permutation, $num_related) {
		$default = 1;
		$matrix = [];
		$permutation = array_values($permutation);

		for($i = 0; $i < $num_related - 1; $i++) {
			$j = $i + 1;

			$rels = isset($permutation[$i]) ? $permutation[$i] : array_fill(0, $num_related - $j, $default);
			for($ii = 0; $ii < $num_related - $j; $ii++) {
				$jj = $ii + $j;
				$val = isset($rels[$ii]) ? $rels[$ii] : $default;
				$matrix[$i][$jj] = $val;
				$matrix[$jj][$i] = 1 / $val; // the opposite direction for the relation
			}
		}
		for($i = 0; $i < $num_related; $i++) {
			$matrix[$i][$i] = 1; // relation with self
		}
		foreach($matrix as $s => $rels) {
			ksort($matrix[$s]);
		}

		return $matrix;
	}

	// Refers to http://mat.gsia.cmu.edu/classes/mstc/multiple/node4.html
	protected function matrixRatioToRelative($matrix) {
		$num = count($matrix);
		$col_sum = [];
		for($j = 0; $j < $num; $j++) {
			$col_sum[$j] = array_sum(array_pluck($matrix, $j));
		}

		$relative_matrix = [];
		for($i = 0; $i < $num; $i++) {
			for($j = 0; $j < $num; $j++) {
				$relative_matrix[$i][$j] = $matrix[$i][$j] / $col_sum[$j];
			}
		}

		$relative = [];
		for($i = 0; $i < $num; $i++) {
			$relative[$i] = array_sum($relative_matrix[$i]) / count($relative_matrix[$i]);
		}

		return array($relative_matrix, $relative);
	}

	public function decide() {
		// From ratios to relative
		list($relative_weights_matrix, $relative_weights) = $this->matrixRatioToRelative($this->weights);

		// From ratios to relative
		$relative_scores_matrix = [];
		$relative_scores = [];
		for($i = 0; $i < count($this->scores); $i++) {
			list($relative_scores_matrix[$i], $relative_scores[$i]) = $this->matrixRatioToRelative($this->scores[$i]);
		}

		// Get the overall (final) score for each job
		$overall_score = [];
		for($i = 0; $i < count($this->goals); $i++) {
			$overall_score[$i] = 0;
			for($j = 0; $j < count($this->criterias); $j++) {
				$overall_score[$i] += $relative_scores[$j][$i] * $relative_weights[$j];
			}
		}

		// Combine the goals and scores, and get the max
		$overall_score = array_combine($this->goals, $overall_score);
		$max = collect($overall_score)->max();
		$key = array_search($max, $overall_score);

		return $key;
	}

	public function matrixToTable($matrix) {
		$html = '<table border="1"><tbody>';
		foreach($matrix as $rels) {
			$html .= '<tr>';
			foreach($rels as $score) {
				$html .= '<td style="width: 100px;">'.$score.'</td>';
			}
			$html .= '</tr>';
		}
		$html .= '</tbody></table>';
		return $html;
	}
}
