<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Okupasi extends Model
{
    //
    protected $table = 'okupasi';
    public $timestamps = false;

    public function profil(){
    	return $this->belongsTo(ProfilLulusan::class,'profil_lulusan_id');
    }
}