<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\RanahTopik;
use Lang;

class RanahTopikController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('ranah-topik.index');
	}

	public function create(Request $request)
	{
		return view('ranah-topik.add', ['model' => new RanahTopik()]);
	}

	public function edit(RanahTopik $ranah_topik)
	{
		return view('ranah-topik.add', ['model' => $ranah_topik]);
	}

	public function show(RanahTopik $ranah_topik)
	{
		return view('ranah-topik.show', ['model' => $ranah_topik]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['id','nama'];

		$query   = RanahTopik::select($columns);

		if($search['value']){
			$query->where(function($query) use ($search, $columns){
				foreach($columns as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columns[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('ranah-topik.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('ranah-topik.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$id     = $request->input('id');
		$unique = $id? 'ranah_topik,nama,'.$id : 'ranah_topik,nama';

		$this->validate($request, [
			'nama' => 'required|unique:'.$unique,
		]);
	}

	private function setAttributes(RanahTopik $item, Request $request){
		$item->nama = $request->nama;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = RanahTopik::findOrFail($request->input('id'));
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('ranah-topik.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('ranah-topik.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new RanahTopik();
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('ranah-topik.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('ranah-topik.edit', $item->id);
	}

	public function destroy(RanahTopik $ranah_topik) {
		$ranah_topik->delete();
		
		return "OK";
	}


}