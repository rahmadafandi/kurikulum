<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\RelTopikMk;
use App\MataKuliah;
use App\RanahTopik;

use Lang;

class RelTopikMkController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('rel-topik-mk.index');
	}

	public function create(Request $request)
	{
		$kuliah = MataKuliah::select('id','nama')->get();
		$topik = RanahTopik::select('id','nama')->get();
		return view('rel-topik-mk.add', ['model' => new RelTopikMk(),'topik'=>$topik,'kuliah'=>$kuliah]);
	}

	public function edit(RelTopikMk $rel_topik_mk)
	{
		$kuliah = MataKuliah::select('id','nama')->get();
		$topik = RanahTopik::select('id','nama')->get();
		$rel = RelTopikMk::where('topik_id','=',$rel_topik_mk->topik_id)->get();
		return view('rel-topik-mk.add', ['model' => $rel_topik_mk,'topik'=>$topik,'kuliah'=>$kuliah,'rel'=>$rel]);
	}

	public function show(RelTopikMk $rel_topik_mk)
	{
		return view('rel-topik-mk.show', ['model' => $rel_topik_mk]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['rel_topik_mk.id','ranah_topik.nama','mata_kuliah.nama as kuliah'];
		$columnsSearch = ['rel_topik_mk.id','ranah_topik.nama','mata_kuliah.nama'];

		$query   = RelTopikMk::select($columns)->join('ranah_topik','ranah_topik.id','=','rel_topik_mk.topik_id')->join('mata_kuliah','mata_kuliah.id','=','rel_topik_mk.mk_id');

		if($search['value']){
			$query->where(function($query) use ($search, $columnsSearch){
				foreach($columnsSearch as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columnsSearch[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('rel-topik-mk.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('rel-topik-mk.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$this->validate($request, [
			'topik_id' => 'required',
			'mk_id' => 'required',
		]);
	}

	private function setAttributes(RelTopikMk $item, $request){	
		$item->topik_id = $request->topik_id;
		$item->mk_id = $request->mk_id;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$del = RelTopikMk::where('topik_id','=',$request->topik_id)->get();
		foreach ($del as $key) {
			$key->delete();
		}
		foreach ($request->mk_id as $value) {
			$item = new RelTopikMk();
			$r = (object) ['topik_id'=>$request->topik_id,'mk_id'=>$value];
			$this->setAttributes($item, $r);
			$status = $item->save();
		}
		if($status)
			return redirect()->route('rel-topik-mk.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('rel-topik-mk.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$status = null;
		foreach ($request->mk_id as $value) {
			$item = new RelTopikMk();
			$r = (object) ['topik_id'=>$request->topik_id,'mk_id'=>$value];
			$this->setAttributes($item, $r);
			$status = $item->save();
		}

		if($status)
			return redirect()->route('rel-topik-mk.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('rel-topik-mk.edit', $item->id);
	}

	public function destroy(RelTopikMk $rel_topik_mk) {
		$rel_topik_mk->delete();
		
		return "OK";
	}


}