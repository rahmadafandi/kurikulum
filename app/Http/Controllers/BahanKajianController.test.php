<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AHP\AHP;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\BahanKajian;

use DB;

class BahanKajianController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
		// Try example
		// https://en.wikipedia.org/wiki/Analytic_hierarchy_process_%E2%80%93_car_example
		$goals = [
			'Accord Sedan',
			'Accord Hybrid',
			'Pilot SUV',
			'CR-V SUV',
			'Element SUV',
			'Odyssey Minivan'
		];
		$criterias = [
			[
				'name'		=> 'cost',
				'relation'	=> [3, 7, 3],
				'sub'		=> [
					[ 'name' => 'purchase',		'relation' => [2, 5, 3] ],
					[ 'name' => 'fuel',			'relation' => [2, 2] ],
					[ 'name' => 'maintenance',	'relation' => [1/2] ],
					[ 'name' => 'resale' ]
				]
			], [
				'name'		=> 'safety',
				'relation'	=> [9, 1]
			], [
				'name'		=> 'style',
				'relation'	=> [1/7]
			], [
				'name'		=> 'capacity',
				'sub'		=> [
					[ 'name' => 'cargo',	'relation' => [1/5] ],
					[ 'name' => 'passenger' ]
				]
			]
		];
		$scores = [
			'cost.purchase'		=> [
				[9, 9, 1, 1/2, 5],
				[1, 1/9, 1/9, 1/7],
				[1/9, 1/9, 1/7],
				[1/2, 5],
				[6]
			],
			'cost.fuel'			=> [
				[1/1.13, 1.41, 1.15, 1.24, 1.19],
				[1.59, 1.30, 1.40, 1.35],
				[1/1.23, 1/1.14, 1/1.18],
				[1.08, 1.04],
				[1/1.04]
			],
			'cost.maintenance'	=> [
				[1.5, 4, 4, 4, 5],
				[4, 4, 4, 5],
				[1, 1.2, 1],
				[1, 3],
				[2]
			],
			'cost.resale'		=> [
				[3, 4, 1/2, 2, 2],
				[2, 1/5, 1, 1],
				[1/6, 1/2, 1/2],
				[4, 4],
				[1]
			],
			'safety'	=> [
				[1, 5, 7, 9, 1/3],
				[5, 7, 9, 1/3],
				[2, 9, 1/8],
				[2, 1/8],
				[1/9]
			],
			'style'		=> [
				[1, 7, 5, 9, 6],
				[7, 5, 9, 6],
				[1/6, 3, 1/3],
				[7, 5],
				[1/5]
			],
			'capacity.cargo'		=> [
				[1, 1/2, 1/2, 1/2, 1/3],
				[1/2, 1/2, 1/2, 1/3],
				[1, 1, 1/2],
				[1, 1/2],
				[1/2]
			],
			'capacity.passenger'	=> [
				[1, 1/2, 1, 3, 1/2],
				[1/2, 1, 3, 1/2],
				[2, 6, 1],
				[3, 1/2],
				[1/6]
			],
		];
		$ahp = new AHP($goals, $criterias, $scores);
		$result = $ahp->decide(true);
		dd(array_dot($scores), $result);
	    return view('bahan-kajian.index', []);
	}

	public function create(Request $request)
	{
	    return view('bahan-kajian.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$bahan_kajian = BahanKajian::findOrFail($id);
	    return view('bahan-kajian.add', [
	        'model' => $bahan_kajian	    ]);
	}

	public function show(Request $request, $id)
	{
		$bahan_kajian = BahanKajian::findOrFail($id);
	    return view('bahan-kajian.show', [
	        'model' => $bahan_kajian	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM bahan_kajian a ";
		if($_GET['search']['value']) {
			$presql .= " WHERE nama LIKE '%".$_GET['search']['value']."%' ";
		}

		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$bahan_kajian = null;
		if($request->id > 0) { $bahan_kajian = BahanKajian::findOrFail($request->id); }
		else {
			$bahan_kajian = new BahanKajian;
		}

		$bahan_kajian->id = $request->id?:0;


		$bahan_kajian->nama = $request->nama;


		$bahan_kajian->deskripsi = $request->deskripsi;

	    	    //$bahan_kajian->user_id = $request->user()->id;
	    $bahan_kajian->save();

	    return redirect()->route('bahan-kajian.index');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {

		$bahan_kajian = BahanKajian::findOrFail($id);
		// dd($bahan_kajian);

		$bahan_kajian->delete();
		return "OK";

	}


}