<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PermissionRole;
use App\Permission;
use App\Role;

use DB;

class PermissionRoleController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('permission-role.index', []);
	}

	public function create(Request $request)
	{
		$role = Role::select('roles.id','roles.display_name')
		->leftJoin('permission_role', 'roles.id', '=', 'permission_role.role_id')
		->leftJoin('permissions', 'permissions.id', '=', 'permission_role.permission_id')
		->where('permission_role.role_id',null)
		->get();
		$permissions = Permission::select('id','display_name')->get();
		// dd($permissions);
	    return view('permission-role.add', ['role'=>$role,'permissions'=>$permissions]);
	}

	public function edit(Request $request, $id)
	{
		$permission_role = PermissionRole::where('role_id','=',$id)->get();
		$role = Role::select('roles.id','roles.display_name')
		->where('roles.id',$id)
		->get();
		$permissions = Permission::select('id','display_name')->get();
	    return view('permission-role.add', [
	        'model' => $permission_role,'role'=>$role,'permissions'=>$permissions]);
	}

	public function show(Request $request, $id)
	{
		$permission_role = PermissionRole::findOrFail($id);
	    return view('permission-role.show', [
	        'model' => $permission_role	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT a.permission_id,a.role_id,b.name,c.display_name,1,2 ";
		$presql = " FROM permission_role a join permissions b on a.permission_id = b.id join roles c on a.role_id = c.id";
		if($_GET['search']['value']) {	
			$presql .= " WHERE b.name LIKE '%".$_GET['search']['value']."%' or c.display_name LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.role_id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
	    DB::table('permission_role')->where('role_id', '=', $request->role_id)->delete();
	    foreach ($request->permission_id as $value) {
	    	DB::table('permission_role')->insert(
		    	['role_id' => $request->role_id, 'permission_id' => $value, ]
			);
	    }
	    return redirect()->route('permission-role.index');

	}

	public function store(Request $request)
	{
		foreach ($request->permission_id as $value) {
	    	DB::table('permission_role')->insert(
		    	['role_id' => $request->role_id, 'permission_id' => $value, ]
			);
	    }
	    return redirect()->route('permission-role.index');
	}

	public function destroy(Request $request, $id,$id2) {
		
	    DB::table('permission_role')->where('role_id', '=', $id)->where('permission_id', '=', $id2)->delete();
		return "OK";
	    
	}

	
}