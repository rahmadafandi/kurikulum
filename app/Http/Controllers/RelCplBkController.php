<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\RelCplBk;
use App\Cpl;
use App\BahanKajian;

use Lang;

class RelCplBkController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('rel-cpl-bk.index');
	}

	public function create(Request $request)
	{
		$cpl = Cpl::select('id','deskripsi')->get();
		$kajian = BahanKajian::select('id','nama')->get();
		return view('rel-cpl-bk.add', ['model' => new RelCplBk(),'cpl'=>$cpl,'kajian'=>$kajian]);
	}

	public function edit(RelCplBk $rel_cpl_bk)
	{
		$cpl = Cpl::select('id','deskripsi')->get();
		$kajian = BahanKajian::select('id','nama')->get();
		$rel = RelCplBk::where('cpl_id','=',$rel_cpl_bk->cpl_id)->get();
		return view('rel-cpl-bk.add', ['model' => $rel_cpl_bk,'cpl'=>$cpl,'kajian'=>$kajian,'rel'=>$rel]);
	}

	public function show(RelCplBk $rel_cpl_bk)
	{
		return view('rel-cpl-bk.show', ['model' => $rel_cpl_bk]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['rel_cpl_bk.id','cpl.deskripsi','bahan_kajian.nama'];

		$query   = RelCplBk::select($columns)->join('cpl','cpl.id','=','rel_cpl_bk.cpl_id')->join('bahan_kajian','bahan_kajian.id','=','rel_cpl_bk.bk_id');

		if($search['value']){
			$query->where(function($query) use ($search, $columns){
				foreach($columns as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columns[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('rel-cpl-bk.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('rel-cpl-bk.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$this->validate($request, [
			'cpl_id' => 'required',
			'bk_id' => 'required',
		]);
	}

	private function setAttributes(RelCplBk $item, $request){
		$item->cpl_id = $request->cpl_id;
		$item->bk_id = $request->bk_id;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$del = RelCplBk::where('cpl_id','=',$request->cpl_id)->get();
		foreach ($del as $key) {
			$key->delete();
		}
		foreach ($request->bk_id as $value) {
			$item = new RelCplBk();
			$r = (object) ['cpl_id'=>$request->cpl_id,'bk_id'=>$value];
			$this->setAttributes($item, $r);
			$status = $item->save();
		}
		if($status)
			return redirect()->route('rel-cpl-bk.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('rel-cpl-bk.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$status = null;
		foreach ($request->bk_id as $value) {
			$item = new RelCplBk();
			$r = (object) ['cpl_id'=>$request->cpl_id,'bk_id'=>$value];
			$this->setAttributes($item, $r);
			$status = $item->save();
		}

		if($status)
			return redirect()->route('rel-cpl-bk.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('rel-cpl-bk.edit', $item->id);
	}

	public function destroy(RelCplBk $rel_cpl_bk) {
		$rel_cpl_bk->delete();
		
		return "OK";
	}


}