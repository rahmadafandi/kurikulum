<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Cpl;
use App\JenisCpl;
use App\Okupasi;
use Lang;

class CplController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('cpl.index');
	}

	public function create(Request $request)
	{
		$jenis = JenisCpl::select('id','nama')->get();
		$okupasi = Okupasi::select('id','nama')->get();
		return view('cpl.add', [
			'model' => new Cpl(),
			'jenis'=>$jenis,
			'okupasi'=>$okupasi
		]);
	}

	public function edit(Cpl $cpl)
	{
		$jenis = JenisCpl::select('id','nama')->get();
		$okupasi = Okupasi::select('id','nama')->get();
		return view('cpl.add', [
			'model' => $cpl,
			'jenis'=>$jenis,
			'okupasi'=>$okupasi
		]);
	}

	public function show(Cpl $cpl)
	{
		return view('cpl.show', ['model' => $cpl]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['cpl.id','jenis_cpl.nama as jenis','okupasi.nama as okupasi','cpl.deskripsi','cpl.bobot'];
		$columnsSearch = ['cpl.id','jenis_cpl.nama','okupasi.nama','cpl.deskripsi','cpl.bobot'];

		$query   = Cpl::select($columns)->join('jenis_cpl','jenis_cpl.id','=','cpl.jenis_id')
		->join('okupasi','okupasi.id','=','cpl.okupasi_id');

		if($search['value']){
			$query->where(function($query) use ($search, $columnsSearch){
				foreach($columnsSearch as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columnsSearch[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('cpl.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('cpl.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$this->validate($request, [
			'jenis_id' => 'required',
			'okupasi_id' => 'required',
			'deskripsi' => 'required',
			'bobot' => 'required|numeric',
		]);
	}

	private function setAttributes(Cpl $item, Request $request){
		$item->jenis_id = $request->jenis_id;
		$item->okupasi_id = $request->okupasi_id;
		$item->deskripsi = $request->deskripsi;
		$item->bobot = $request->bobot;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = Cpl::findOrFail($request->input('id'));
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('cpl.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('cpl.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new Cpl();
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('cpl.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('cpl.edit', $item->id);
	}

	public function destroy(Cpl $cpl) {
		$cpl->delete();
		
		return "OK";
	}


}