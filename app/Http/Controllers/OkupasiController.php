<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Okupasi;
use App\ProfilLulusan;
use Lang;

class OkupasiController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('okupasi.index');
	}

	public function create(Request $request)
	{
		$profil = ProfilLulusan::select('id','nama')->get();
		return view('okupasi.add', ['model' => new Okupasi(),'profil'=>$profil]);
	}

	public function edit(Okupasi $okupasi)
	{
		$profil = ProfilLulusan::select('id','nama')->get();
		return view('okupasi.add', ['model' => $okupasi,'profil'=>$profil]);
	}

	public function show(Okupasi $okupasi)
	{
		return view('okupasi.show', ['model' => $okupasi]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['okupasi.id','okupasi.nama','okupasi.deskripsi','profil_lulusan.nama as profil'];
		$columnsSearch = ['okupasi.id','okupasi.nama','okupasi.deskripsi','profil_lulusan.nama'];

		$query   = Okupasi::select($columns)->join('profil_lulusan','okupasi.profil_lulusan_id','=','profil_lulusan.id');

		if($search['value']){
			$query->where(function($query) use ($search, $columnsSearch){
				foreach($columnsSearch as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columnsSearch[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('okupasi.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('okupasi.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$id     = $request->input('id');
		$unique = $id? 'okupasi,nama,'.$id : 'okupasi,nama';

		$this->validate($request, [
			'nama' => 'required|unique:'.$unique,
			'deskripsi' => 'required',
			'profil_lulusan_id' => 'required',
		]);
	}

	private function setAttributes(Okupasi $item, Request $request){
		$item->nama = $request->nama;
		$item->deskripsi = $request->deskripsi;
		$item->profil_lulusan_id = $request->profil_lulusan_id;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = Okupasi::findOrFail($request->input('id'));
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('okupasi.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('okupasi.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new Okupasi();
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('okupasi.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('okupasi.edit', $item->id);
	}

	public function destroy(Okupasi $okupasi) {
		$okupasi->delete();
		
		return "OK";
	}


}