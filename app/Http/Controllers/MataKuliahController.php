<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\MataKuliah;
use App\JenisMk;
use App\LingkupMk;
use App\RanahTopik;
use Lang;

use DB;

class MataKuliahController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('mata-kuliah.index');
	}

	public function create(Request $request)
	{
		$mata_kuliah = new MataKuliah();
		$mata_kuliah['ranah_topik'] = $mata_kuliah->ranah_topik()->orderBy('nama')->get()->implode('nama', ',');

		$jenis = JenisMk::select('id','nama')->get();
		$lingkup = LingkupMk::select('id','nama')->get();
		$ranah_topik = RanahTopik::all();
		return view('mata-kuliah.add', [
			'model' => $mata_kuliah,
			'jenis' => $jenis,
			'lingkup' => $lingkup,
			'ranah_topik' => $ranah_topik,
		]);
	}

	public function edit(MataKuliah $mata_kuliah)
	{
		$mata_kuliah['ranah_topik'] = $mata_kuliah->ranah_topik()->orderBy('nama')->get()->implode('nama', ',');

		$jenis = JenisMk::select('id','nama')->get();
		$lingkup = LingkupMk::select('id','nama')->get();
		$ranah_topik = RanahTopik::all();
		return view('mata-kuliah.add', [
			'model' => $mata_kuliah,
			'jenis' => $jenis,
			'lingkup' => $lingkup,
			'ranah_topik' => $ranah_topik,
		]);
	}

	public function show(MataKuliah $mata_kuliah)
	{
		return view('mata-kuliah.show', ['model' => $mata_kuliah]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['mata_kuliah.id','lingkup_mk.nama as lingkup','jenis_mk.nama as jenis','mata_kuliah.nama','mata_kuliah.deskripsi','mata_kuliah.dependency_path'];
		$columnsSearch = ['mata_kuliah.id','lingkup_mk.nama','jenis_mk.nama','mata_kuliah.nama','mata_kuliah.deskripsi','mata_kuliah.dependency_path'];

		$query   = MataKuliah::select($columns)->join('jenis_mk','mata_kuliah.jenis_id','=','jenis_mk.id')
												->join('lingkup_mk','mata_kuliah.lingkup_id','=','lingkup_mk.id');

		if($search['value']){
			$query->where(function($query) use ($search, $columns){
				foreach($columns as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columns[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('mata-kuliah.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('mata-kuliah.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		// $id     = $request->input('id');
		// $unique = $id? 'mata_kuliah,unique_column,'.$id : 'mata_kuliah,unique_column';

		$this->validate($request, [
			'lingkup_id' => 'required|exists:lingkup_mk,id',
			'jenis_id' => 'required|exists:jenis_mk,id',
			'ranah_topik' => ['required','string','regex:/^([A-Za-z0-9 ]+,)*([A-Za-z0-9 ]+)$/'],
			'nama' => 'required|max:255',
			'deskripsi' => '',
			'dependency_path' => '',
		]);
	}

	private function setAttributes(MataKuliah $item, Request $request){
		$user = Auth::user();

		$item->lingkup_id = $request->lingkup_id;
		$item->jenis_id = $request->jenis_id;
		$item->nama = $request->nama;
		$item->deskripsi = $request->deskripsi;
		$item->dependency_path = $request->dependency_path;

		// if(empty($item->id))
		// 	$item->insert_user_id = $user->id;
		// else
		// 	$item->update_user_id = $user->id;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = MataKuliah::findOrFail($request->input('id'));
		$this->setAttributes($item, $request);

		$failed = false;
		DB::beginTransaction();

		try {
			$failed = !$item->save();

			$topiks = explode(',', $request->ranah_topik);
			$topik_ids = [];
			foreach($topiks as $topik) {
				$topik = RanahTopik::firstOrCreate(['nama' => $topik]);
				$topik_ids[] = $topik->id;
			}
			$item->ranah_topik()->sync($topik_ids);
		} catch(\Exception $e) {
			$failed = true;
			DB::rollback();
			$to = isset($request_id) && $request_id != 0 ? 'mata-kuliah.edit' : 'mata-kuliah.create';
			return redirect()->route($to)
				->withErrors($e->getMessage())
				->withInput();
			// throw $e;
		}

		// Finally
		DB::commit();

		// $status = $item->save();
		if(!$failed)
			return redirect()->route('mata-kuliah.index')->with('status', !$failed)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('mata-kuliah.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new MataKuliah();
		$this->setAttributes($item, $request);

		$failed = false;
		DB::beginTransaction();

		try {
			$failed = !$item->save();

			$topiks = explode(',', $request->ranah_topik);
			$topik_ids = [];
			foreach($topiks as $topik) {
				$topik = RanahTopik::firstOrCreate(['nama' => $topik]);
				$topik_ids[] = $topik->id;
			}
			$item->ranah_topik()->sync($topik_ids);
		} catch(\Exception $e) {
			$failed = true;
			DB::rollback();
			$to = isset($request_id) && $request_id != 0 ? 'mata-kuliah.edit' : 'mata-kuliah.create';
			return redirect()->route($to)
				->withErrors($e->getMessage())
				->withInput();
			// throw $e;
		}

		// Finally
		DB::commit();

		// $status = $item->save();
		if(!$failed)
			return redirect()->route('mata-kuliah.index')->with('status', !$failed)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('mata-kuliah.edit', $item->id);
	}

	public function destroy(MataKuliah $mata_kuliah) {
		$mata_kuliah->ranah_topik()->detach();
		$mata_kuliah->delete();

		return "OK";
	}


}