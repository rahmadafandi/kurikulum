<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\BloomsTaxonomy;
use Lang;

class BloomsTaxonomyController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('blooms-taxonomy.index');
	}

	public function create(Request $request)
	{
		return view('blooms-taxonomy.add', ['model' => new BloomsTaxonomy()]);
	}

	public function edit(BloomsTaxonomy $blooms_taxonomy)
	{
		return view('blooms-taxonomy.add', ['model' => $blooms_taxonomy]);
	}

	public function show(BloomsTaxonomy $blooms_taxonomy)
	{
		return view('blooms-taxonomy.show', ['model' => $blooms_taxonomy]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['id','frasa','level','kata_dasar'];

		$query   = BloomsTaxonomy::select($columns);

		if($search['value']){
			$query->where(function($query) use ($search, $columns){
				foreach($columns as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columns[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('blooms-taxonomy.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('blooms-taxonomy.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$id     = $request->input('id');
		$unique = $id? 'blooms_taxonomy,frasa,'.$id : 'blooms_taxonomy,frasa';

		$this->validate($request, [
			'frasa' => 'required|unique:'.$unique,
			'level' => 'required|integer|max:9',
			'kata_dasar' => 'required',
		]);
	}

	private function setAttributes(BloomsTaxonomy $item, Request $request){
		$item->frasa = $request->frasa;
		$item->level = $request->level;
		$item->kata_dasar = $request->kata_dasar;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = BloomsTaxonomy::findOrFail($request->input('id'));
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('blooms-taxonomy.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('blooms-taxonomy.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new BloomsTaxonomy();
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('blooms-taxonomy.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('blooms-taxonomy.edit', $item->id);
	}

	public function destroy(BloomsTaxonomy $blooms_taxonomy) {
		$blooms_taxonomy->delete();
		
		return "OK";
	}


}