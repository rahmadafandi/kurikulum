<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\UserProdi;
use App\Prodi;
use App\User;
use Lang;

class UserProdiController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('user-prodi.index');
	}

	public function create(Request $request)
	{
		$user = User::doesntHave('user_prodi')->select('id','name')->get(); 
		// User::select('id','name')->get();
		$prodi = Prodi::select('id','nama')->get();
		return view('user-prodi.add', [
			'model' => new UserProdi(),
			'user' => $user,
			'prodi'=> $prodi
		]);
	}

	public function edit(UserProdi $user_prodi)
	{
		$user = User::select('id','name')->get();
		$prodi = Prodi::select('id','nama')->get();
		return view('user-prodi.add', [
			'model' => $user_prodi,
			'user' => $user,
			'prodi'=> $prodi]);
	}

	public function show(UserProdi $user_prodi)
	{
		return view('user-prodi.show', ['model' => $user_prodi]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['users.id','users.name','prodi.nama'];

		$query   = UserProdi::select($columns)->join('users','users.id','=','user_prodi.user_id')
		->join('prodi','prodi.id','=','user_prodi.prodi_id');

		if($search['value']){
			$query->where(function($query) use ($search, $columns){
				foreach($columns as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columns[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('user-prodi.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('user-prodi.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$this->validate($request, [
			'user_id' => 'required',
			'prodi_id' => 'required',
		]);
	}

	private function setAttributes(UserProdi $item, Request $request){

		$item->user_id = $request->user_id;
		$item->prodi_id = $request->prodi_id;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = UserProdi::findOrFail($request->input('user_id'));
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('user-prodi.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('user-prodi.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new UserProdi();
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('user-prodi.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('user-prodi.edit', $item->id);
	}

	public function destroy(UserProdi $user_prodi) {
		$user_prodi->delete();
		
		return "OK";
	}


}