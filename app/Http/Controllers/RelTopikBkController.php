<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\RelTopikBk;
use App\BahanKajian;
use App\RanahTopik;
use Lang;

class RelTopikBkController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('rel-topik-bk.index');
	}

	public function create(Request $request)
	{
		$kajian = BahanKajian::select('id','nama')->get();
		$topik = RanahTopik::select('id','nama')->get();
		return view('rel-topik-bk.add', ['model' => new RelTopikBk(),'topik'=>$topik,'kajian'=>$kajian]);
	}

	public function edit(RelTopikBk $rel_topik_bk)
	{
		$kajian = BahanKajian::select('id','nama')->get();
		$topik = RanahTopik::select('id','nama')->get();
		$rel = RelTopikBk::where('topik_id','=',$rel_topik_bk->topik_id)->get();
		return view('rel-topik-bk.add', ['model' => $rel_topik_bk,'topik'=>$topik,'kajian'=>$kajian,'rel'=>$rel]);
	}

	public function show(RelTopikBk $rel_topik_bk)
	{
		return view('rel-topik-bk.show', ['model' => $rel_topik_bk]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['rel_topik_bk.id','ranah_topik.nama','bahan_kajian.nama as kajian'];
		$columnsSearch = ['rel_topik_bk.id','ranah_topik.nama','bahan_kajian.nama'];

		$query   = RelTopikBk::select($columns)->join('ranah_topik','ranah_topik.id','=','rel_topik_bk.topik_id')->join('bahan_kajian','bahan_kajian.id','=','rel_topik_bk.bk_id');

		if($search['value']){
			$query->where(function($query) use ($search, $columnsSearch){
				foreach($columnsSearch as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columnsSearch[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('rel-topik-bk.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('rel-topik-bk.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$this->validate($request, [
			'topik_id' => 'required',
			'bk_id' => 'required',
		]);
	}

	private function setAttributes(RelTopikBk $item, $request){
		$item->topik_id = $request->topik_id;
		$item->bk_id = $request->bk_id;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$del = RelTopikBk::where('topik_id','=',$request->topik_id)->get();
		foreach ($del as $key) {
			$key->delete();
		}
		foreach ($request->bk_id as $value) {
			$item = new RelTopikBk();
			$r = (object) ['topik_id'=>$request->topik_id,'bk_id'=>$value];
			$this->setAttributes($item, $r);
			$status = $item->save();
		}
		if($status)
			return redirect()->route('rel-topik-bk.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('rel-topik-bk.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$status = null;
		foreach ($request->bk_id as $value) {
			$item = new RelTopikBk();
			$r = (object) ['topik_id'=>$request->topik_id,'bk_id'=>$value];
			$this->setAttributes($item, $r);
			$status = $item->save();
		}
		if($status)
			return redirect()->route('rel-topik-bk.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('rel-topik-bk.edit', $item->id);
	}

	public function destroy(RelTopikBk $rel_topik_bk) {
		$rel_topik_bk->delete();
		
		return "OK";
	}


}