<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\User;
use Lang;

class UserController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('users.index');
	}

	public function create(Request $request)
	{
		return view('users.add', ['model' => new User()]);
	}

	public function edit(User $user)
	{
		return view('users.add', ['model' => $user]);
	}

	public function show(User $user)
	{
		return view('users.show', ['model' => $user]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['id','name','email'];

		$query   = User::select($columns);

		if($search['value']){
			$query->where(function($query) use ($search, $columns){
				foreach($columns as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columns[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('users.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('users.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$id     = $request->input('id');
		$unique = $id? 'users,email,'.$id : 'users,email';

		if (!$id) {
			$this->validate($request, [
				'name' => 'required',
				'email' => 'required|email|unique:'.$unique,
				'password' => 'required'
			]);
		}else{
			$this->validate($request, [
				'name' => 'required',
				'email' => 'required|email|unique:'.$unique
			]);
		}
	}

	private function setAttributes(User $item, Request $request){
		$user = Auth::user();

		$item->name = $request->name;
		$item->email = $request->email;
		if ($request->password) {

			$item->password = bcrypt($request->password);
		}
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = User::findOrFail($request->input('id'));
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('users.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('users.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new User();
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('users.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('users.edit', $item->id);
	}

	public function destroy(User $user) {
		$user->delete();

		return "OK";
	}


}