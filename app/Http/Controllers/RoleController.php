<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Role;

use DB;

class RoleController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('role.index', []);
	}

	public function create(Request $request)
	{
	    return view('role.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$role = Role::findOrFail($id);
	    return view('role.add', [
	        'model' => $role	    ]);
	}

	public function show(Request $request, $id)
	{
		$role = Role::findOrFail($id);
	    return view('role.show', [
	        'model' => $role	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM roles a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE name LIKE '%".$_GET['search']['value']."%' or display_name LIKE '%".$_GET['search']['value']."%' or id LIKE '%".$_GET['search']['value']."%' or description LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$role = null;
		if($request->id > 0) { $role = Role::findOrFail($request->id); }
		else { 
			$role = new Role;
		}
	    

	    		
			    $role->id = $request->id?:0;
				
	    		
					    $role->name = $request->name;
		
	    		
					    $role->display_name = $request->display_name;
		
	    		
					    $role->description = $request->description;
		
	    	    //$role->user_id = $request->user()->id;
	    $role->save();

	    return redirect()->route('role.index');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$role = Role::findOrFail($id);

		$role->delete();
		return "OK";
	    
	}

	
}