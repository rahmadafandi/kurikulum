<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\RoleUser;
use App\Role;
use App\User;

use DB;

class RoleUserController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('role-user.index', []);
	}

	public function create(Request $request)
	{
		$role = User::select('users.id','users.email')
		->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
		->leftJoin('roles', 'users.id', '=', 'role_user.user_id')
		->where('role_user.user_id',null)
		->get();
		// dd($role);
		$roles = Role::select('id','display_name')->get();
	    return view('role-user.add', ['role'=>$role,'roles'=>$roles]);
	}

	public function edit(Request $request, $id)
	{	
		$role_user = RoleUser::where('user_id',$id)->get();
		$role = User::select('users.id','users.email')
		->where('users.id',$id)
		->get();
		// dd($role);
		$roles = Role::select('id','display_name')->get();
	    return view('role-user.add', [
	        'model' => $role_user[0],'role'=>$role,'roles'=>$roles]);
	}

	public function show(Request $request, $id)
	{
		$role_user = RoleUser::where('user_id',$id)->get();
		$results = DB::select('select users.email, roles.display_name from role_user join users on users.id = role_user.user_id join roles on roles.id = role_user.role_id where role_user.user_id ='.$id);
		$model = ['user_id'=>$results[0]->email,'role_id'=>$results[0]->display_name];
	    return view('role-user.show', ['model' => $model]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT a.user_id, a.role_id, b.display_name, c.email,1,2 ";
		$presql = " FROM role_user a join roles b on a.role_id = b.id join users c on c.id = a.user_id";
		if($_GET['search']['value']) {	
			$presql .= " WHERE b.display_name LIKE '%".$_GET['search']['value']."%' or c.email LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.user_id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/

	    // dd($request);
	 	DB::table('role_user')
        ->where('user_id', $request->user_id)
        ->update(['role_id' => $request->role_id]);

	    return redirect()->route('user-role.index');

	}

	public function store(Request $request)
	{
		DB::table('role_user')->insert(
		    ['user_id' => $request->user_id, 'role_id' => $request->role_id, ]
		);
		
	    return redirect()->route('user-role.index');
		// return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		// $role_user = RoleUser::where('user_id',$id)->get();

		// dd($role_user);
		DB::table('role_user')->where('user_id', '=', $id)->delete();
		return "OK";
	    
	}

	
}