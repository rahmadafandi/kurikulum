<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\ProfilLulusan;
use App\Prodi;
use Lang;

class ProfilLulusanController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('profil-lulusan.index');
	}

	public function create(Request $request)
	{
		$prodi = Prodi::select('id','nama')->get();
		return view('profil-lulusan.add', ['model' => new ProfilLulusan(),'prodi'=>$prodi]);
	}

	public function edit(ProfilLulusan $profil_lulusan)
	{
		$prodi = Prodi::select('id','nama')->get();
		return view('profil-lulusan.add', ['model' => $profil_lulusan,'prodi'=>$prodi]);
	}

	public function show(ProfilLulusan $profil_lulusan)
	{
		return view('profil-lulusan.show', ['model' => $profil_lulusan]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['profil_lulusan.id','prodi.nama as prodi','profil_lulusan.nama','profil_lulusan.deskripsi'];
		$columnsSearch = ['profil_lulusan.id','prodi.nama','profil_lulusan.nama','profil_lulusan.deskripsi'];

		$query   = ProfilLulusan::select($columns)->join('prodi','profil_lulusan.prodi_id','=','prodi.id');

		if($search['value']){
			$query->where(function($query) use ($search, $columnsSearch){
				foreach($columnsSearch as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columnsSearch[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('profil-lulusan.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('profil-lulusan.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		// $id     = $request->input('id');
		// $unique = $id? 'profil_lulusan,unique_column,'.$id : 'profil_lulusan,unique_column';

		$this->validate($request, [
															'prodi_id' => 'required',
												'nama' => 'required',
												'deskripsi' => 'required',
								]);
	}

	private function setAttributes(ProfilLulusan $item, Request $request){
		$user = Auth::user();

										$item->prodi_id = $request->prodi_id;
								$item->nama = $request->nama;
								$item->deskripsi = $request->deskripsi;
				
		// if(empty($item->id))
		// 	$item->insert_user_id = $user->id;
		// else
		// 	$item->update_user_id = $user->id;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = ProfilLulusan::findOrFail($request->input('id'));
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('profil-lulusan.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('profil-lulusan.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new ProfilLulusan();
		$this->setAttributes($item, $request);

		$status = $item->save();
		if($status)
			return redirect()->route('profil-lulusan.index')->with('status', $status)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('profil-lulusan.edit', $item->id);
	}

	public function destroy(ProfilLulusan $profil_lulusan) {
		$profil_lulusan->delete();
		
		return "OK";
	}


}