<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\BahanKajian;
use App\RanahTopik;
use Lang;
use DB;

class BahanKajianController extends Controller
{
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}


	public function index(Request $request)
	{
		return view('bahan-kajian.index');
	}

	public function create(Request $request)
	{
		$bahan_kajian = new BahanKajian();
		$bahan_kajian['ranah_topik'] = $bahan_kajian->ranah_topik()->orderBy('nama')->get()->implode('nama', ',');
		$ranah_topik = RanahTopik::all();
		return view('bahan-kajian.add', [
			'model' => $bahan_kajian,
			'ranah_topik' => $ranah_topik
		]);
	}

	public function edit(BahanKajian $bahan_kajian)
	{
		$ranah_topik = RanahTopik::all();
		$bahan_kajian['ranah_topik'] = $bahan_kajian->ranah_topik()->orderBy('nama')->get()->implode('nama', ',');

		return view('bahan-kajian.add', [
			'model' => $bahan_kajian,
			'ranah_topik' => $ranah_topik
		]);
	}

	public function show(BahanKajian $bahan_kajian)
	{
		return view('bahan-kajian.show', ['model' => $bahan_kajian]);
	}

	public function grid(Request $request)
	{
		$length = $request->get('length');
		$start  = $request->get('start');
		$orders = $request->get('order');
		$search = $request->get('search');
		$columns = ['id','nama','deskripsi'];

		$query   = BahanKajian::select($columns);

		if($search['value']){
			$query->where(function($query) use ($search, $columns){
				foreach($columns as $column){
					$query->orWhere($column, 'like', '%'.$search['value'].'%');
				}
			});
		}

		foreach($orders as $order){
			$query->orderBy($columns[$order['column']], $order['dir']);
		}

		$result = new \stdClass();
		$result->iTotalDisplayRecords = $query->count();
		$result->data  = $query->take($length)->skip($start)->get();

		foreach($result->data as &$item){
			$html = array();

			$html[] = '<div class="mini ui buttons">';

			$html[] = '<a href="'.route('bahan-kajian.show', $item->id).'" class="ui blue button">';
			$html[] = '<i class="file alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="'.route('bahan-kajian.edit', $item->id).'" class="ui yellow button">';
			$html[] = '<i class="edit icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '<a href="#" onclick="return doDelete('.$item->id.')" class="ui red button">';
			$html[] = '<i class="trash alternate icon" style="margin:0px"></i>';
			$html[] = '</a>';

			$html[] = '</div>';

			$item->buttons = implode('', $html);
		}

		echo json_encode($result);
	}

	private function validateForm(Request $request){
		$id     = $request->input('id');
		$unique = $id? 'bahan_kajian,nama,'.$id : 'bahan_kajian,nama';

		$this->validate($request, [
			'nama' => 'required|unique:'.$unique,
			'deskripsi' => '',
			'ranah_topik' => ['required','string','regex:/^([A-Za-z0-9 ]+,)*([A-Za-z0-9 ]+)$/'],
		]);
	}

	private function setAttributes(BahanKajian $item, Request $request){
		$item->nama = $request->nama;
		$item->deskripsi = $request->deskripsi;
	}


	public function update(Request $request) {
		$this->validateForm($request);

		$item = BahanKajian::findOrFail($request->input('id'));
		$this->setAttributes($item, $request);

		$failed = false;
		DB::beginTransaction();

		try {
			$failed = !$item->save();

			$topiks = explode(',', $request->ranah_topik);
			$topik_ids = [];
			foreach($topiks as $topik) {
				$topik = RanahTopik::firstOrCreate(['nama' => $topik]);
				$topik_ids[] = $topik->id;
			}
			$item->ranah_topik()->sync($topik_ids);
		} catch(\Exception $e) {
			$failed = true;
			DB::rollback();
			$to = isset($request_id) && $request_id != 0 ? 'bahan-kajian.edit' : 'bahan-kajian.create';
			return redirect()->route($to)
				->withErrors($e->getMessage())
				->withInput();
			// throw $e;
		}

		// Finally
		DB::commit();

		// $status = $item->save();
		if(!$failed)
			return redirect()->route('bahan-kajian.index')->with('status', !$failed)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('bahan-kajian.edit', $item->id);

	}

	public function store(Request $request)
	{
		$this->validateForm($request);

		$item = new BahanKajian();
		$this->setAttributes($item, $request);

		$failed = false;
		DB::beginTransaction();

		try {
			$failed = !$item->save();

			$topiks = explode(',', $request->ranah_topik);
			$topik_ids = [];
			foreach($topiks as $topik) {
				$topik = RanahTopik::firstOrCreate(['nama' => $topik]);
				$topik_ids[] = $topik->id;
			}
			$item->ranah_topik()->sync($topik_ids);
		} catch(\Exception $e) {
			$failed = true;
			DB::rollback();
			$to = isset($request_id) && $request_id != 0 ? 'bahan-kajian.edit' : 'bahan-kajian.create';
			return redirect()->route($to)
				->withErrors($e->getMessage())
				->withInput();
			// throw $e;
		}

		// Finally
		DB::commit();

		// $status = $item->save();
		if(!$failed)
			return redirect()->route('bahan-kajian.index')->with('status', !$failed)->with('messages', Lang::get('app.save_success'));
		else
			return redirect()->route('bahan-kajian.edit', $item->id);
	}

	public function destroy(BahanKajian $bahan_kajian) {
		$bahan_kajian->delete();

		return "OK";
	}


}