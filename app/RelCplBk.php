<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelCplBk extends Model
{
    //
    protected $table = 'rel_cpl_bk';
    public $timestamps = false;

    public function cpl(){
    	return $this->belongsTo(Cpl::class,'cpl_id');
    }

    public function bk(){
    	return $this->belongsTo(BahanKajian::class,'bk_id');
    }
}