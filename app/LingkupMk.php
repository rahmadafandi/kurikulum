<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LingkupMk extends Model
{
    //
    protected $table = 'lingkup_mk';
    public $timestamps = false;

    public function mata_kuliah()
    {
        return $this->hasMany('App\MataKuliah');
    }
}
