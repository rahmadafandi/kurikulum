<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisMk extends Model
{
    //
    protected $table = 'jenis_mk';
    public $timestamps = false;

    public function mata_kuliah()
    {
        return $this->hasMany('App\MataKuliah');
    }
}