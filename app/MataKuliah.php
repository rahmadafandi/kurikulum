<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    //
    protected $table = 'mata_kuliah';
    public $timestamps = false;

    public function jenis(){
    	return $this->belongsTo(JenisMk::class,'jenis_id');
    }

    public function lingkup()
    {
        return $this->belongsTo(LingkupMk::class, 'lingkup_id');
    }

    public function ranah_topik()
    {
        return $this->belongsToMany('App\RanahTopik', 'rel_topik_mk', 'mk_id', 'topik_id');
    }
}