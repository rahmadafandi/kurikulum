<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilLulusan extends Model
{
    //
    protected $table = 'profil_lulusan';
    public $timestamps = false;

    public function prodi(){
    	return $this->belongsTo(Prodi::class,'prodi_id');
    }
}