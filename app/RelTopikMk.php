<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelTopikMk extends Model
{
    //
    protected $table = 'rel_topik_mk';
    public $timestamps = false;

 	public function mk(){
    	return $this->belongsTo(MataKuliah::class,'mk_id');
    }

    public function topik(){
    	return $this->belongsTo(RanahTopik::class,'topik_id');
    }
}