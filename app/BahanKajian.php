<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahanKajian extends Model
{
    //
    protected $table = 'bahan_kajian';
    public $timestamps = false;

    public function ranah_topik()
    {
        return $this->belongsToMany('App\RanahTopik', 'rel_topik_bk', 'bk_id', 'topik_id');
    }

    public function cpl()
    {
        return $this->belongsToMany('App\CPL', 'rel_cpl_bk', 'bk_id', 'cpl_id');
    }
}