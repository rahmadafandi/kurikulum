<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BloomsTaxonomy extends Model
{
    //
    protected $table = 'blooms_taxonomy';
    public $timestamps = false;

}