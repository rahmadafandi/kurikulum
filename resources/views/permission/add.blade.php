@extends('layouts.master')

@section('content')

<div class="ui card fluid">
  <div class="content">
    <a class="header">Permission</a>
    <div class="meta">
      <span>@if(isset($model)) Modify @else Add @endif Permission</span>
    </div>
    <div class="description">
        <form action="{{ route('permission.index')}}{{( isset($model) ? "/" . $model->id : "") }}" method="POST" class="ui form">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


            <input type="hidden" name="id" id="id" value="{{$model['id'] or ''}}" readonly="readonly">
            <div class="field">
                <label>Name</label>
                <input type="text" name="name" id="name" value="{{$model['name'] or ''}}">
            </div>
            <div class="field">
                <label>Display Name</label>
                <input type="text" name="display_name" id="display_name" value="{{$model['display_name'] or ''}}">
            </div>
            <div class="field">
                <label>Description</label>
                <input type="text" name="description" id="description" value="{{$model['description'] or ''}}">
            </div>
                                                
            <button type="submit" class="ui primary button"><i class="plus square icon"></i> Save</button>
            <a class="ui red button" href="{{ route('permission.index') }}"><i class="hand point left outline icon"></i> Back</a>
        </form>
    </div>
  </div>
</div>
@endsection