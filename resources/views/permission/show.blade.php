@extends('layouts.master')

@section('content')

<div class="ui card fluid">
  <div class="content">
    <a class="header">Permission</a>
    <div class="meta">
      <span>View Permission</span>
    </div>
    <div class="description">
        <form action="{{ route('permission.index') }}" method="POST" class="ui form">


                
        <div class="field">
            <label>ID</label>
            <input type="text" name="id" id="id" value="{{$model['id'] or ''}}" readonly="readonly">
        </div>
        
                
        <div class="field">
            <label>Name</label>
            <input type="text" name="name" id="name" value="{{$model['name'] or ''}}" readonly="readonly">
        </div>
        
                
        <div class="field">
            <label>Display Name</label>
            <input type="text" name="display_name" id="display_name" value="{{$model['display_name'] or ''}}" readonly="readonly">
        </div>
        
                
        <div class="field">
            <label>Description</label>
            <input type="text" name="description" id="description" value="{{$model['description'] or ''}}" readonly="readonly">
        </div>

        <a class="ui red button" href="{{ route('permission.index') }}"><i class="hand point left outline icon"></i> Back</a>


        </form>
    </div>
  </div>
</div>
@endsection