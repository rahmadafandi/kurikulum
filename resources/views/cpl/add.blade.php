@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('cpl.index') }}" class="section">{{ Lang::get('app.cpl') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->id))
			<div class="section">{{$model->okupasi->nama}} -> {{$model->jenis->nama}} </div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.cpl') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->id)? route('cpl.store') : route('cpl.update', $model->id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->id))
				<input type="hidden" name="_method" value="PUT">
				@endif

				<input type="hidden" name="id" id="id" value="{{ $model->id }}" readonly="readonly">
				<div class="field">
					<label>{{ Lang::get('app.jenis_cpl') }}</label>
					<select class="ui search dropdown" name="jenis_id" id="jenis_id">
						<option value="">{{ Lang::get('app.jenis_cpl') }}</option>
						@foreach($jenis as $val)
						<option value="{{ $val->id }}" {{ $model->jenis_id == $val->id? 'selected' : '' }}>
							{{ $val->nama }}
						</option>
						@endforeach
					</select>
				</div>
				{!! $errors->has('jenis_id')?'<div class="ui error message"> <p> '.$errors->first('jenis_id').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.okupasi') }}</label>
					<select class="ui search dropdown" name="okupasi_id" id="okupasi_id">
						<option value="">{{ Lang::get('app.okupasi') }}</option>
						@foreach($okupasi as $val)
						<option value="{{ $val->id }}" {{ $model->okupasi_id == $val->id? 'selected' : '' }}>
							{{ $val->nama }}
						</option>
						@endforeach
					</select>
				</div>
				{!! $errors->has('okupasi_id')?'<div class="ui error message"> <p> '.$errors->first('okupasi_id').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.deskripsi') }}</label>
					<input type="text" name="deskripsi" id="deskripsi" value="{{ old('deskripsi', $model->deskripsi) }}">
				</div>
				{!! $errors->has('deskripsi')?'<div class="ui error message"> <p> '.$errors->first('deskripsi').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.bobot') }}</label>
					<input type="text" name="bobot" id="bobot" value="{{ old('bobot', $model->bobot) }}">
				</div>
				{!! $errors->has('bobot')?'<div class="ui error message"> <p> '.$errors->first('bobot').'</p> </div>':'' !!}

				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('cpl.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection