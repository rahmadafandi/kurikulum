<!DOCTYPE html>
<html>
<head>
	<title>{{ Lang::get('app.kurikulum') }}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	@yield('meta')
	<link rel="stylesheet" href="{{ asset('semantic/semantic.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/dataTables.semanticui.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/highchart/highcharts.css') }}">
	<link rel="stylesheet" href="{{ asset('css/responsive.semanticui.min.css') }}">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	<script>
		var Lang = {
			app : {!! json_encode(Lang::get('app')) !!},
			datatable : {!! json_encode(Lang::get('datatable')) !!}
		};
	</script>

	<style type="text/css">
	.ui.sidebar.visible ~ .pusher{
		padding-left: 260px;
	}
	.ui.sidebar ~ .pusher{
		padding-top: 70px;
	}
	.ui.vertical.menu .accordion .title{
		color: rgba(255,255,255,.5);
		padding: 5px 0px;
	}
	.ui.vertical.menu .accordion .title:hover,
	.ui.vertical.menu .accordion .title.active{
		color: rgba(255,255,255,.9);
	}
	.ui.vertical.menu .accordion .content{
		padding: 5px 0px;
	}
	.ui.vertical.menu .accordion .item{
		font-size: 1em;
	}
</style>
@yield('styles')
</head>
<body>
	<!-- Horizontal Menu -->
	<div class="ui fixed top attached inverted menu" style="height:65px; background-color: #C50A0A; margin-top:0;">
		@if(Auth::check())<a class="item" id="menu">
			<i class="large exchange icon" style="color:white; width:10px;"></i>
		</a>
		@endif
		<a href="" style="padding-top:7px;">
			<h3 class="ui header" style="color:#fff;">
				<img alt="" height="90%" src="{{ asset('img/icon.png')}}" style="margin-top:5px;margin-left: 10px;">
				{{ Lang::get('app.kurikulum') }}
			</h3>
		</a>
		@if(Auth::check())
		<div class="right menu">
			<a class="item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
				<i class="fa fa-sign-out"></i> {{ Lang::get('auth.logout') }}
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
		</div>
		@endif
	</div>

	<!-- Vertical Menu -->
	@if(Auth::check())
	<div class="ui visible left sidebar inverted vertical menu overlay" style="margin-top:64px !important;display:block; width: 240px; height:91% !important; background: #A20000;">
		<div class="item">
			<div class="header">{{ Lang::get('app.komponen_kurikulum') }}</div>
			<div class="menu">
				<a href="{{ route('bahan-kajian.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.bahan_kajian') }}
				</a>
				<a href="{{ route('blooms-taxonomy.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.blooms_taxonomy') }}
				</a>
				<a href="{{ route('cpl.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.cpl') }}
				</a>
				<a href="{{ route('mata-kuliah.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.mata_kuliah') }}
				</a>
				<a href="{{ route('okupasi.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.okupasi') }}
				</a>
				<a href="{{ route('prodi.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.prodi') }}
				</a>
				<a href="{{ route('profil-lulusan.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.profil_lulusan') }}
				</a>
				<a href="{{ route('ranah-topik.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.ranah_topik') }}
				</a>
				<a href="{{ route('rel-cpl-bk.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.rel_cpl_bk') }}
				</a>
				<a href="{{ route('rel-topik-bk.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.rel_topik_bk') }}
				</a>
				<a href="{{ route('rel-topik-mk.index') }}" class="item">
					<i class="fa fa-map-signs"></i> {{ Lang::get('app.rel_topik_mk') }}
				</a>
			</div>
		</div>
		@permission('superadmin')
		<div class="item">
			<div class="header">{{ Lang::get('app.user_management') }}</div>
			<div class="menu">
				<a class="item" href="{{ route('users.index') }}">
					<i class="fa fa-user-circle-o"></i> {{ Lang::get('app.user') }}
				</a>
				<a class="item" href="{{ route('role.index') }}">
					<i class="fa fa-user-secret"></i> {{ Lang::get('app.role') }}
				</a>
				<a class="item" href="{{ route('permission.index') }}">
					<i class="fa fa-user-times"></i> {{ Lang::get('app.permission') }}
				</a>
				<a class="item" href="{{ route('user-role.index') }}">
					<i class="fa fa-link"></i> {{ Lang::get('app.user_role') }}
				</a>
				<a class="item" href="{{ route('permission-role.index') }}">
					<i class="fa fa-unlink"></i> {{ Lang::get('app.role_permission') }}
				</a>
			</div>
		</div>
		@endpermission
	</div>
	@endif

	<div class="pusher">
		<div style="padding: 10px">@yield('content')</div>
	</div>

	<!-- Scripts -->
	<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.appear.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/dataTables.semanticui.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/responsive.semanticui.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('semantic/semantic.min.js') }}"></script>
	<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$(document).ready(function() {
			try{
				$('.ui.left.sidebar').sidebar({
					transition: 'overlay',
					dimPage: false,
					closable: false
				});

				$('.ui.dropdown:not(.multiple)').dropdown();
				$('.ui.accordion').accordion();
				$('.ui.left.sidebar').sidebar('attach events', '#menu');
			}
			catch(err){
			}
		});
	</script>
	@yield('scripts')
</body>
</html>

<!DOCTYPE html>