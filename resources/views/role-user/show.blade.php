@extends('layouts.master')

@section('content')

<div class="ui card fluid">
  <div class="content">
    <a class="header">User Role</a>
    <div class="meta">
      <span>View User Role</span>
    </div>
    <div class="description">
        <form action="{{ route('user-role.index') }}" method="POST" class="ui form">


                
        <div class="field">
            <label>User</label>
            <input type="text" name="user_id" id="user_id" value="{{$model['user_id'] or ''}}" readonly="readonly">
        </div>
        
                
        <div class="field">
            <label>Role</label>
            <input type="text" name="role_id" id="role_id" value="{{$model['role_id'] or ''}}" readonly="readonly">
        </div>
        
        
        <a class="ui red button" href="{{ route('user-role.index') }}"><i class="hand point left outline icon"></i> Back</a>


        </form>
    </div>
  </div>
</div>
@endsection