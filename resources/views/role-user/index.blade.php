@extends('layouts.master')

@section('content')
<div class="ui card fluid">
  <div class="content">
    <a class="header">User Role</a>
    <div class="meta">
      <span>List of User Role</span>
    </div>
    <div class="description">
		<table class="ui celled table" cellspacing="0" width="100%" id="thegrid">
		  <thead>
		    <tr>
		        		        <th>User</th>
		        		        <th>Role</th>
		        		        <th style="width:50px"></th>
		        <th style="width:50px"></th>
		    </tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
    </div>
  </div>
  <div class="extra content">
    <a href="{{route('user-role.create')}}" class="ui primary button">Add</a>
  </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{route('user-role.index')}}/grid",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return row[3];
                        },
                        "targets": 0
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ route('user-role.index') }}/'+row[0]+'">'+row[2]+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ route('user-role.index') }}/'+row[0]+'/edit" class="ui green button">Update</a>';
                        },
                        "targets": 2
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="ui red button">Delete</a>';
                        },
                        "targets": 2+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ route('user-role.index') }}/' + id, type: 'DELETE'}).done(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection