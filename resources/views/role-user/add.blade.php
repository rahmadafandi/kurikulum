@extends('layouts.master')

@section('content')

<div class="ui card fluid">
  <div class="content">
    <a class="header">User Role</a>
    <div class="meta">
      <span>@if(isset($model)) Modify @else Add @endif User Role</span>
    </div>
    <div class="description">
        <form action="{{ route('user-role.index')}}{{( isset($model) ? "/" . $model->id : "") }}" method="POST" class="ui form">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


            <div class="field">
                <label>User</label>
                @if(count($role)==0)
                <input type="text" id="user_id" value="Tidak ada user tersedia" readonly="readonly">
                @else
                <input type="hidden" name="user_id" id="user_id" value="{{ (isset($model)? $model['user_id']:'') }}" readonly="readonly">
                <select class="ui search dropdown" name="user_id" id="user_id" {{ (isset($model)?'disabled="disabled"':'') }}>
                    <option value="">User</option>
                @foreach($role as $val)
                  <option value="{{ $val->id }}" {{ (isset($model)?(($model['user_id']==$val->id)?'selected':''):'') }}>{{ $val->email }}</option>
                @endforeach
                </select>
                @endif
            </div>
            <div class="field">
                <label>Role</label>
                @if(count($roles)==0)
                <input type="text" id="role_id" value="Tidak ada role tersedia" readonly="readonly">
                @else
                <select class="ui search dropdown" name="role_id" id="role_id">
                    <option value="">Role</option>
                @foreach($roles as $val)
                  <option value="{{ $val->id }}" {{ (isset($model)?(($model['role_id']==$val->id)?'selected':''):'') }}>{{ $val->display_name }}</option>
                @endforeach
                </select>
                @endif
            </div>
                                                            
            <button type="submit" class="ui primary button"><i class="plus square icon"></i> Save</button>
            <a class="ui red button" href="{{ route('user-role.index') }}"><i class="hand point left outline icon"></i> Back</a>
        </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('select.dropdown')
          .dropdown()
        ;
    });
</script>
@endsection