@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('permissions.index') }}" class="section">{{ Lang::get('app.permissions') }}</a>
			<i class="right angle icon divider"></i>
			<div class="section">Name</div>
			<i class="right angle icon divider"></i>
			<div class="active section">{{ Lang::get('app.detail') }}</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			<table class="ui celled table">
				<tbody>
										<tr>
						<td class="collapsing active">{{ Lang::get('app.id') }}</td>
						<td>{{ $model->id }}</td>
					</tr>
										<tr>
						<td class="collapsing active">{{ Lang::get('app.name') }}</td>
						<td>{{ $model->name }}</td>
					</tr>
										<tr>
						<td class="collapsing active">{{ Lang::get('app.display_name') }}</td>
						<td>{{ $model->display_name }}</td>
					</tr>
										<tr>
						<td class="collapsing active">{{ Lang::get('app.description') }}</td>
						<td>{{ $model->description }}</td>
					</tr>
										<tr>
						<td class="collapsing active">{{ Lang::get('app.created_at') }}</td>
						<td>{{ $model->created_at }}</td>
					</tr>
										<tr>
						<td class="collapsing active">{{ Lang::get('app.updated_at') }}</td>
						<td>{{ $model->updated_at }}</td>
					</tr>
									</tbody>
			</table>
			<!-- Buttons -->
			<div class="ui small buttons">
				<a class="ui yellow button" href="{{ route('permissions.edit', $model->id) }}">
					<i class="edit icon"></i> {{ Lang::get('app.edit') }}
				</a>
				<a class="ui button" href="{{ route('permissions.index') }}">
					<i class="hand point left outline icon"></i> {{ Lang::get('app.back') }}
				</a>
			</div>
		</div>
	</div>
</div>
@endsection