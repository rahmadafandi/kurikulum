@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('blooms-taxonomy.index') }}" class="section">{{ Lang::get('app.blooms_taxonomy') }}</a>
			<i class="right angle icon divider"></i>
			<div class="section">{{ $model->frasa }}</div>
			<i class="right angle icon divider"></i>
			<div class="active section">{{ Lang::get('app.detail') }}</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			<table class="ui celled table">
				<tbody>
					<tr>
						<td class="collapsing active">{{ Lang::get('app.id') }}</td>
						<td>{{ $model->id }}</td>
					</tr>
					<tr>
						<td class="collapsing active">{{ Lang::get('app.frasa') }}</td>
						<td>{{ $model->frasa }}</td>
					</tr>
					<tr>
						<td class="collapsing active">{{ Lang::get('app.level') }}</td>
						<td>{{ $model->level }}</td>
					</tr>
					<tr>
						<td class="collapsing active">{{ Lang::get('app.kata_dasar') }}</td>
						<td>{{ $model->kata_dasar }}</td>
					</tr>
				</tbody>
			</table>
			<!-- Buttons -->
			<div class="ui small buttons">
				<a class="ui yellow button" href="{{ route('blooms-taxonomy.edit', $model->id) }}">
					<i class="edit icon"></i> {{ Lang::get('app.edit') }}
				</a>
				<a class="ui button" href="{{ route('blooms-taxonomy.index') }}">
					<i class="hand point left outline icon"></i> {{ Lang::get('app.back') }}
				</a>
			</div>
		</div>
	</div>
</div>
@endsection