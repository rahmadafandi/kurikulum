@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('blooms-taxonomy.index') }}" class="section">{{ Lang::get('app.blooms_taxonomy') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->id))
			<div class="section">{{ $model->frasa }}</div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.blooms_taxonomy') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->id)? route('blooms-taxonomy.store') : route('blooms-taxonomy.update', $model->id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->id))
				<input type="hidden" name="_method" value="PUT">
				@endif

				<input type="hidden" name="id" id="id" value="{{ $model->id }}" readonly="readonly">
				<div class="field">
					<label>{{ Lang::get('app.frasa') }}</label>
					<input type="text" name="frasa" id="frasa" value="{{ old('frasa', $model->frasa) }}">
				</div>
				{!! $errors->has('frasa')?'<div class="ui error message"> <p> '.$errors->first('frasa').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.level') }}</label>
					<input type="text" name="level" id="level" value="{{ old('level', $model->level) }}">
				</div>
				{!! $errors->has('level')?'<div class="ui error message"> <p> '.$errors->first('level').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.kata_dasar') }}</label>
					<input type="text" name="kata_dasar" id="kata_dasar" value="{{ old('kata_dasar', $model->kata_dasar) }}">
				</div>
				{!! $errors->has('kata_dasar')?'<div class="ui error message"> <p> '.$errors->first('kata_dasar').'</p> </div>':'' !!}
				
				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('blooms-taxonomy.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection