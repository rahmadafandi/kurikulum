@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<div class="active section">{{ Lang::get('app.blooms_taxonomy') }}</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="extra content">
		<div class="ui grid">
			<div class="three wide column">
				<a href="{{ route('blooms-taxonomy.create')}}" class="ui small green button">
					<i class="plus square icon"></i> {{ Lang::get('app.add') }}
				</a>
			</div>
			<div class="thirteen wide column">
				<!-- Messages -->
				@php $messages = session('messages'); @endphp
				@if($messages)
				@if(is_array($messages))
				<div class="ui small @if(session('status') == 1) positive @else negative @endif message" role="alert">
					<ul>
						@foreach($messages as $message)
						<li>{{ $message }}</li>
						@endforeach
					</ul>
				</div>
				@else
				<div class="ui small @if(session('status') == 1) positive @else negative @endif message" role="alert">
					{{ session('messages') }}
				</div>
				@endif
				@endif
			</div>
		</div>
	</div>
	<div class="content">
		<div class="description">
			<table class="ui celled red table responsive" cellspacing="0" width="100%" id="thegrid">
				<thead>
					<tr>
						<th>{{ Lang::get('app.id') }}</th>
						<th>{{ Lang::get('app.frasa') }}</th>
						<th>{{ Lang::get('app.level') }}</th>
						<th>{{ Lang::get('app.kata_dasar') }}</th>
						<th style="width:50px"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var theGrid = null;
	$(document).ready(function(){
		theGrid = $('#thegrid').DataTable({
			'info'      : false,
			'drawCallback': function (settings) { 
				$(".dataTables_filter").each(function () { 
					$(this).find("label").addClass('ui input field'); 
				}); 
			},
			'ordering'  : true,
			'processing': true,
			'responsive': true,
			'serverSide': true,
			'ajax'      : {
				url: "{{ route('blooms-taxonomy.index')}}/grid",
				dataSrc: function(result){
					console.log(result);
					return result.data;
				}
			},
			columns: [
			{ data: 'id', name: 'id', searchable: false , width: '20px'},
			{ data: 'frasa', name: 'frasa' },
			{ data: 'level', name: 'level' },
			{ data: 'kata_dasar', name: 'kata_dasar' },
			{ data: 'buttons', name: 'buttons', orderable: false, searchable: false },
			],
			'language'  : {
				'emptyTable'    : Lang.datatable.no_data,
				'infoEmpty'     : Lang.datatable.no_data,
				'zeroRecords'   : Lang.datatable.no_data,
				'thousands'     : ',',
				'lengthMenu'    : Lang.datatable.showing_n_data,
				'loadingRecords': Lang.datatable.loading_data,
				'processing'    : Lang.datatable.processing_data,
				'search'        : "",
				'searchPlaceholder' : Lang.app.search,
				'paginate': {
					'first'     : '<<',
					'last'      : '>>',
					'next'      : '>',
					'previous'  : '<'
				}
			}
		});
	});

	function doDelete(id) {
		if(confirm('Apakah anda yakin ingin menghapus data ini?')) {
			$.ajax({ url: '{{ route('blooms-taxonomy.index') }}/' + id, type: 'DELETE'}).done(function() {
				theGrid.ajax.reload();
			});
		}
		return false;
	}
</script>
@endsection