@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('user-prodi.index') }}" class="section">{{ Lang::get('app.user_prodi') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->user_id))
			<div class="section">{{ $model->user->name}}</div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->user_id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.user_prodi') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->user_id)? route('user-prodi.store') : route('user-prodi.update', $model->user_id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->user_id))
				<input type="hidden" name="_method" value="PUT">
				@endif

				<div class="field">
					<label>{{ Lang::get('app.user') }}</label>
					<select class="ui search dropdown" name="user_id" id="user_id">
						<option value="">{{ Lang::get('app.users') }}</option>
						@foreach($user as $val)
						<option value="{{ $val->id }}" {{ $model->user_id == $val->id? 'selected' : '' }}>
							{{ $val->name }}
						</option>
						@endforeach
					</select>
				</div>
				{!! $errors->has('user_id')?'<div class="ui error message"> <p> '.$errors->first('user_id').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.prodi') }}</label>
					<select class="ui search dropdown" name="prodi_id" id="prodi_id">
						<option value="">{{ Lang::get('app.prodi') }}</option>
						@foreach($prodi as $val)
						<option value="{{ $val->id }}" {{ $model->prodi_id == $val->id? 'selected' : '' }}>
							{{ $val->nama }}
						</option>
						@endforeach
					</select>
				</div>
				{!! $errors->has('prodi_id')?'<div class="ui error message"> <p> '.$errors->first('prodi_id').'</p> </div>':'' !!}

				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('user-prodi.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection