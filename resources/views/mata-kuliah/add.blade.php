@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('mata-kuliah.index') }}" class="section">{{ Lang::get('app.mata_kuliah') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->id))
			<div class="section">{{$model->nama}}</div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.mata_kuliah') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->id)? route('mata-kuliah.store') : route('mata-kuliah.update', $model->id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->id))
				<input type="hidden" name="_method" value="PUT">
				@endif

				<input type="hidden" name="id" id="id" value="{{ $model->id }}" readonly="readonly">
				<div class="field">
					<label>{{ Lang::get('app.lingkup_mk') }}</label>
					<select class="ui search dropdown" name="lingkup_id" id="lingkup_id">
						<option value="">{{ Lang::get('app.lingkup_mk') }}</option>
						@foreach($lingkup as $val)
						<option value="{{ $val->id }}" {{ old('lingkup_id', $model->lingkup_id) == $val->id? 'selected' : '' }}>
							{{ $val->nama }}
						</option>
						@endforeach
					</select>
				</div>
				{!! $errors->has('lingkup_id')?'<div class="ui error message"> <p> '.$errors->first('lingkup_id').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.jenis_mk') }}</label>
					<select class="ui search dropdown" name="jenis_id" id="jenis_id">
						<option value="">{{ Lang::get('app.jenis_mk') }}</option>
						@foreach($jenis as $val)
						<option value="{{ $val->id }}" {{ old('jenis_id', $model->jenis_id) == $val->id? 'selected' : '' }}>
							{{ $val->nama }}
						</option>
						@endforeach
					</select>
				</div>
				{!! $errors->has('jenis_id')?'<div class="ui error message"> <p> '.$errors->first('jenis_id').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.ranah_topik') }}</label>
					<div class="ui fluid multiple search selection dropdown">
						<input name="ranah_topik" id="ranah_topik" type="hidden" value="{{ old('ranah_topik', $model['ranah_topik']) }}">
						<i class="dropdown icon"></i>
						<div class="default text">Ranah Topik</div>
						<div class="menu">
						@foreach($ranah_topik as $val)
							<div class="item" data-value="{{ $val->nama }}" {{ preg_match('/\b'.$val->nama.'\b/', old('ranah_topik', $model['ranah_topik'])) ? 'selected="selected"' : '' }}>{{ $val->nama }}</div>
						@endforeach
						</div>
					</div>
					<label class="help-text">Pilih dari opsi yang ada, atau tambahkan yang baru dan pisahkan dengan koma</label>
				</div>
				{!! $errors->has('ranah_topik')?'<div class="ui error message"> <p> '.$errors->first('ranah_topik').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.nama') }}</label>
					<input type="text" name="nama" id="nama" value="{{ old('nama', $model->nama) }}">
				</div>
				{!! $errors->has('nama')?'<div class="ui error message"> <p> '.$errors->first('nama').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.deskripsi') }}</label>
					<textarea name="deskripsi" id="deskripsi" rows="2">{{ old('deskripsi', $model->deskripsi) }}</textarea>
				</div>
				{!! $errors->has('deskripsi')?'<div class="ui error message"> <p> '.$errors->first('deskripsi').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.dependency_path') }}</label>
					<input type="text" name="dependency_path" id="dependency_path" value="{{ old('dependency_path', $model->dependency_path) }}">
				</div>
				{!! $errors->has('dependency_path')?'<div class="ui error message"> <p> '.$errors->first('dependency_path').'</p> </div>':'' !!}

				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('mata-kuliah.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('.ui.dropdown.multiple').dropdown({
			allowAdditions: true
		});
	});
</script>
@endsection