@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('users.index') }}" class="section">{{ Lang::get('app.users') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->id))
			<div class="section">{{ $model->name }}</div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.users') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->id)? route('users.store') : route('users.update', $model->id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->id))
				<input type="hidden" name="_method" value="PUT">
				@endif

				<input type="hidden" name="id" id="id" value="{{ $model->id }}" readonly="readonly">
				<div class="field">
					<label>{{ Lang::get('app.name') }}</label>
					<input type="text" name="name" id="name" value="{{ old('name', $model->name) }}">
				</div>
				{!! $errors->has('name')?'<div class="ui error message"> <p> '.$errors->first('name').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.email') }}</label>
					<input type="text" name="email" id="email" value="{{ old('email', $model->email) }}">
				</div>
				{!! $errors->has('email')?'<div class="ui error message"> <p> '.$errors->first('email').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.password') }}</label>
					<input type="password" name="password" id="password" value="">
				</div>
				{!! $errors->has('password')?'<div class="ui error message"> <p> '.$errors->first('password').'</p> </div>':'' !!}


				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('users.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection