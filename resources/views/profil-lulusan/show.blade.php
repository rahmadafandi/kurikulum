@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('profil-lulusan.index') }}" class="section">{{ Lang::get('app.profil_lulusan') }}</a>
			<i class="right angle icon divider"></i>
			<div class="section">{{ $model->nama }}</div>
			<i class="right angle icon divider"></i>
			<div class="active section">{{ Lang::get('app.detail') }}</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			<table class="ui celled table">
				<tbody>
					<tr>
						<td class="collapsing active">{{ Lang::get('app.id') }}</td>
						<td>{{ $model->id }}</td>
					</tr>
					<tr>
						<td class="collapsing active">{{ Lang::get('app.prodi') }}</td>
						<td>{{ $model->prodi->nama }}</td>
					</tr>
					<tr>
						<td class="collapsing active">{{ Lang::get('app.nama') }}</td>
						<td>{{ $model->nama }}</td>
					</tr>
					<tr>
						<td class="collapsing active">{{ Lang::get('app.deskripsi') }}</td>
						<td>{{ $model->deskripsi }}</td>
					</tr>
				</tbody>
			</table>
			<!-- Buttons -->
			<div class="ui small buttons">
				<a class="ui yellow button" href="{{ route('profil-lulusan.edit', $model->id) }}">
					<i class="edit icon"></i> {{ Lang::get('app.edit') }}
				</a>
				<a class="ui button" href="{{ route('profil-lulusan.index') }}">
					<i class="hand point left outline icon"></i> {{ Lang::get('app.back') }}
				</a>
			</div>
		</div>
	</div>
</div>
@endsection