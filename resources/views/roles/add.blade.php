@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('roles.index') }}" class="section">{{ Lang::get('app.roles') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->id))
			<div class="section">Name</div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.roles') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->id)? route('roles.store') : route('roles.update', $model->id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->id))
				<input type="hidden" name="_method" value="PUT">
				@endif

												<input type="hidden" name="id" id="id" value="{{ $model->id }}" readonly="readonly">
																																				<div class="field">
					<label>{{ Lang::get('app.name') }}</label>
					<input type="text" name="name" id="name" value="{{ old('name', $model->name) }}">
				</div>
				{!! $errors->has('name')?'<div class="ui error message"> <p> '.$errors->first('name').'</p> </div>':'' !!}
																																<div class="field">
					<label>{{ Lang::get('app.display_name') }}</label>
					<input type="text" name="display_name" id="display_name" value="{{ old('display_name', $model->display_name) }}">
				</div>
				{!! $errors->has('display_name')?'<div class="ui error message"> <p> '.$errors->first('display_name').'</p> </div>':'' !!}
																																<div class="field">
					<label>{{ Lang::get('app.description') }}</label>
					<input type="text" name="description" id="description" value="{{ old('description', $model->description) }}">
				</div>
				{!! $errors->has('description')?'<div class="ui error message"> <p> '.$errors->first('description').'</p> </div>':'' !!}
																																												<div class="field">
					<label>{{ Lang::get('app.created_at') }}</label>
					<input type="text" name="created_at" id="created_at" value="{{ old('created_at', $model->created_at) }}">
				</div>
				{!! $errors->has('created_at')?'<div class="ui error message"> <p> '.$errors->first('created_at').'</p> </div>':'' !!}
																																<div class="field">
					<label>{{ Lang::get('app.updated_at') }}</label>
					<input type="text" name="updated_at" id="updated_at" value="{{ old('updated_at', $model->updated_at) }}">
				</div>
				{!! $errors->has('updated_at')?'<div class="ui error message"> <p> '.$errors->first('updated_at').'</p> </div>':'' !!}
								
				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('roles.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection