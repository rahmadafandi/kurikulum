@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('ranah-topik.index') }}" class="section">{{ Lang::get('app.ranah_topik') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->id))
			<div class="section">Name</div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.ranah_topik') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->id)? route('ranah-topik.store') : route('ranah-topik.update', $model->id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->id))
				<input type="hidden" name="_method" value="PUT">
				@endif

												<input type="hidden" name="id" id="id" value="{{ $model->id }}" readonly="readonly">
																																				<div class="field">
					<label>{{ Lang::get('app.nama') }}</label>
					<input type="text" name="nama" id="nama" value="{{ old('nama', $model->nama) }}">
				</div>
				{!! $errors->has('nama')?'<div class="ui error message"> <p> '.$errors->first('nama').'</p> </div>':'' !!}
																				
				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('ranah-topik.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection