@extends('layouts.master')

@section('styles')
<style type="text/css">
.pusher{
	padding-top: 70px;
	margin-top: 150px;
}
</style>
@endsection
@section('content')
<div class="ui grid centered">
	<div class="eight wide computer sixteen wide mobile column">
		<div class="ui red card fluid">
			<div class="content">				
				<div class="header">{{ __('Login') }}</div>
				<div class="description">
					<form action="{{ route('login') }}" method="POST" class="ui form">
						@csrf
						<div class="field">
							<label>{{ __('E-Mail Address') }}</label>
							<input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
						</div>
						@if ($errors->has('email'))
						<div class="ui error message">
							<div class="header">Error</div>
							<p>{{ $errors->first('email') }}</p>
						</div>
						@endif
						<div class="field">
							<label>{{ __('Password') }}</label>
							<input id="password" type="password" name="password" required>
						</div>
						@if ($errors->has('password'))
						<div class="ui error message">
							<div class="header">Error</div>
							<p>{{ $errors->first('password') }}</p>
						</div>
						@endif

						<button class="ui green button" type="submit">Login</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
