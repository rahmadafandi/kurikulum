@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('bahan-kajian.index') }}" class="section">{{ Lang::get('app.bahan_kajian') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->id))
			<div class="section">{{ $model->nama}}</div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.bahan_kajian') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->id)? route('bahan-kajian.store') : route('bahan-kajian.update', $model->id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->id))
				<input type="hidden" name="_method" value="PUT">
				@endif

				<input type="hidden" name="id" id="id" value="{{ $model->id }}" readonly="readonly">
				<div class="field">
					<label>{{ Lang::get('app.nama') }}</label>
					<input type="text" name="nama" id="nama" value="{{ old('nama', $model->nama) }}">
				</div>
				{!! $errors->has('nama')?'<div class="ui error message"> <p> '.$errors->first('nama').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.deskripsi') }}</label>
					<input type="text" name="deskripsi" id="deskripsi" value="{{ old('deskripsi', $model->deskripsi) }}">
				</div>
				{!! $errors->has('deskripsi')?'<div class="ui error message"> <p> '.$errors->first('deskripsi').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.ranah_topik') }}</label>
					<div class="ui fluid multiple search selection dropdown">
						<input name="ranah_topik" id="ranah_topik" type="hidden" value="{{ old('ranah_topik', $model['ranah_topik']) }}">
						<i class="dropdown icon"></i>
						<div class="default text">Ranah Topik</div>
						<div class="menu">
						@foreach($ranah_topik as $val)
							<div class="item" data-value="{{ $val->nama }}" {{ preg_match('/\b'.$val->nama.'\b/', old('ranah_topik', $model['ranah_topik'])) ? 'selected="selected"' : '' }}>{{ $val->nama }}</div>
						@endforeach
						</div>
					</div>
				</div>
				{!! $errors->has('ranah_topik')?'<div class="ui error message"> <p> '.$errors->first('ranah_topik').'</p> </div>':'' !!}

				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('bahan-kajian.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('.ui.dropdown.multiple').dropdown({
			allowAdditions: false
		});
	});
</script>
@endsection