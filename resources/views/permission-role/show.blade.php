@extends('layouts.master')

@section('content')

<div class="ui card fluid">
  <div class="content">
    <a class="header">Permission_role</a>
    <div class="meta">
      <span>View Permission_role</span>
    </div>
    <div class="description">
        <form action="{{ url('/permission_roles') }}" method="POST" class="ui form">


                
        <div class="field">
            <label>Permission Id</label>
            <input type="text" name="permission_id" id="permission_id" value="{{$model['permission_id'] or ''}}" readonly="readonly">
        </div>
        
                
        <div class="field">
            <label>Role Id</label>
            <input type="text" name="role_id" id="role_id" value="{{$model['role_id'] or ''}}" readonly="readonly">
        </div>
        
        
        <a class="ui red button" href="{{ url('/permission_roles') }}"><i class="hand point left outline icon"></i> Back</a>


        </form>
    </div>
  </div>
</div>
@endsection