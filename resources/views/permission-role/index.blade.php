@extends('layouts.master')

@section('content')
<div class="ui card fluid">
  <div class="content">
    <a class="header">Permission Role</a>
    <div class="meta">
      <span>List of Permission Role</span>
    </div>
    <div class="description">
		<table class="ui celled table" cellspacing="0" width="100%" id="thegrid">
		  <thead>
		    <tr>
		        <th>Permission</th>
		        <th>Role</th>
		        <th style="width:50px"></th>
		    </tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
    </div>
  </div>
  <div class="extra content">
    <a href="{{ route('permission-role.create')}}" class="ui primary button">Add</a>
  </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{route('permission-role.index')}}/grid",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return row[2];
                        },
                        "targets": 0
                    },{
                        "render": function ( data, type, row ) {
                            return row[3];
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ route('permission-role.index') }}/'+row[1]+'/edit" class="ui green button">Update</a>';
                        },
                        "targets": 2
                    },
                ]
            });
        });
    </script>
@endsection