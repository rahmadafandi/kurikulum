@extends('layouts.master')

@section('content')

<div class="ui card fluid">
	<div class="content">
		<a class="header">Permission Role</a>
		<div class="meta">
			<span>@if(isset($model)) Modify @else Add @endif Permission Role</span>
		</div>
		<div class="description">
			<form action="{{ route('permission-role.index')}}{{( isset($model) ? "/" . $model[0]->role_id : "") }}" method="POST" class="ui form">
				{{ csrf_field() }}

				@if (isset($model))
				<input type="hidden" name="_method" value="PATCH">
				@endif


				<div class="field">
					<label>Role</label>
					@if(count($role)==0)
					<input type="text" id="role_id" value="Tidak ada role tersedia" readonly="readonly">
					@else
					<input type="hidden" name="role_id" id="role_id" value="{{ (isset($model)? $model[0]['role_id']:'') }}" readonly="readonly">
					<select class="ui search dropdown" name="role_id" id="role_id" {{ (isset($model)?'disabled="disabled"':'') }}>
						<option value="">Role</option>
						@foreach($role as $val)
						<option value="{{ $val->id }}" {{ (isset($model)?(($model[0]['role_id']==$val->id)?'selected':''):'') }}>{{ $val->display_name }}</option>
						@endforeach
					</select>
					@endif
				</div>
				<div class="field">
					<label>Permission</label>
					@if(count($permissions)==0)
					<input type="text" id="permission_id" class="form-control" value="Tidak ada permission tersedia" readonly="readonly">
					@else
					<select class="ui search dropdown" name="permission_id[]" id="permission_id" multiple="multiple">
						<option value="">Permission</option>
						@foreach($permissions as $val)
						@if(isset($model))
						<option value="{{ $val->id }}" @foreach($model as $myval) {{ (($myval['permission_id']==$val->id)?'selected':'') }} @endforeach>{{ $val->display_name }}</option>
						@else
						<option value="{{ $val->id }}" {{ (isset($model)?(($model['permission_id']==$val->id)?'selected':''):'') }}>{{ $val->display_name }}</option>
						@endif
						@endforeach
					</select>
					@endif
				</div>
				
				<button type="submit" class="ui primary button"><i class="plus square icon"></i> Save</button>
				<a class="ui red button" href="{{ route('permission-role.index') }}"><i class="hand point left outline icon"></i> Back</a>
			</form>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('select.dropdown')
		.dropdown()
		;
	});
</script>
@endsection