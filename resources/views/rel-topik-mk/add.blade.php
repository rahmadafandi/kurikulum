@extends('layouts.master')

@section('content')
<div class="ui card fluid">
	<div class="content">
		<div class="ui breadcrumb">
			<div class="section">{{ Lang::get('app.home') }}</div>
			<i class="right angle icon divider"></i>
			<a href="{{ route('rel-topik-mk.index') }}" class="section">{{ Lang::get('app.rel_topik_mk') }}</a>
			<i class="right angle icon divider"></i>
			@if(!empty($model->id))
			<div class="section">{{ $model->topik->nama }}</div>
			<i class="right angle icon divider"></i>
			@endif
			<div class="active section">
				@if(!empty($model->id)) {{ Lang::get('app.edit') }} @else {{ Lang::get('app.add') }} @endif
				{{ Lang::get('app.rel_topik_mk') }}
			</div>
		</div>
	</div>
</div>
<div class="ui card fluid">
	<div class="content">
		<div class="description">
			@php $action = empty($model->id)? route('rel-topik-mk.store') : route('rel-topik-mk.update', $model->id); @endphp
			<form action="{{ $action }}" method="POST" class="ui form @if($errors) error @endif">
				{{ csrf_field() }}

				@if(!empty($model->id))
				<input type="hidden" name="_method" value="PUT">
				@endif

				<input type="hidden" name="id" id="id" value="{{ $model->id }}" readonly="readonly">
				<div class="field">
					<label>{{ Lang::get('app.topik') }}</label>
					<select class="ui search dropdown" name="topik_id" id="topik_id">
						<option value="">{{ Lang::get('app.topik') }}</option>
						@foreach($topik as $val)
						<option value="{{ $val->id }}" {{ $model->topik_id == $val->id? 'selected' : '' }}>
							{{ $val->nama }}
						</option>
						@endforeach
					</select>
				</div>
				{!! $errors->has('topik_id')?'<div class="ui error message"> <p> '.$errors->first('topik_id').'</p> </div>':'' !!}
				<div class="field">
					<label>{{ Lang::get('app.mata_kuliah') }}</label>
					<select class="ui search dropdown" name="mk_id[]" id="mk_id" multiple="multiple">
						<option value="">{{ Lang::get('app.mata_kuliah') }}</option>
						@foreach($kuliah as $val)
						@if(!empty($model->id))
						<option value="{{ $val->id }}"  @foreach($rel as $myval) {{ (($myval->mk_id==$val->id)?'selected':'') }} @endforeach >
							{{ $val->nama }}
						</option>
						@else
						<option value="{{ $val->id }}" {{ $model->mk_id == $val->id? 'selected' : '' }}>
							{{ $val->nama }}
						</option>
						@endif
						@endforeach
					</select>
				</div>
				{!! $errors->has('mk_id')?'<div class="ui error message"> <p> '.$errors->first('mk_id').'</p> </div>':'' !!}

				<div class="ui small buttons">
					<button type="submit" class="ui green button">
						<i class="check square icon"></i> {{ Lang::get('app.save') }}
					</button>
					<a class="ui yellow button" href="{{ route('rel-topik-mk.index') }}">
						<i class="hand point left outline icon"></i> {{ Lang::get('app.cancel') }}
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection