<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Password harus lebih panjang dari enam karakter dan cocok dengan konfirmasi.',
    'reset' => 'Password Anda berhasil dirubah!',
    'sent' => 'Kami telah mengirim link untuk merubah password ke email Anda!',
    'token' => 'Token ganti password ini tidak valid.',
    'user' => 'Kami tidak bisa menemukan user dengan email tersebut.',

];
