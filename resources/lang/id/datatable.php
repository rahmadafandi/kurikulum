<?php

return [
    'loading_data'      => 'Sedang memuat data...',
    'no_data'           => 'Tidak ada data yang tersedia.',
    'processing_data'   => 'Sedang memproses data...',
    'showing_n_data'    => 'Menampilkan _MENU_ data.',
];
