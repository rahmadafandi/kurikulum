<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'    => 'Login gagal. Mohon pastikan email dan passwod Anda sudah benar.',
    'logout'    => 'Log Out',
    'throttle'  => 'Terlalu banyak perintah login. Mohon untuk mencoba lagi setelah :seconds detik.',

];
